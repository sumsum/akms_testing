import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import org.openqa.selenium.interactions.Actions as Actions

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName'), Username)

WebUI.setText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), Password)

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(5)

//Click and verify Loan History
if (Username == 'AT_AKMS_OAKAdmin') {
    WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Loan History1'))
} else {
    WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Loan History'))
}

WebUI.verifyElementText(findTestObject('Page_AKMS - Loan History/b_Loan History'), 'Loan History')

WebUI.delay(2)

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

Random numGenerator = new Random()

int rowNum = numGenerator.nextInt(rows_table.size())

WebElement row = rows_table.get(rowNum)

List<WebElement> columns = row.findElements(By.tagName('td'))

rowNum = numGenerator.nextInt(rows_table.size())

row = rows_table.get(rowNum)

columns = row.findElements(By.tagName('td'))

//Get the Loan History information for specific records
Access_Kit_Type = columns.get(0).getText()

Access_Kit_Name = columns.get(1).getText()

Borrow_Time = columns.get(2).getText()

Return_Time = columns.get(3).getText()

Applicant_Name = columns.get(5).getText()

WebUI.comment(Access_Kit_Type)

WebUI.comment(Access_Kit_Name)

WebUI.comment(Borrow_Time)

WebUI.comment(Return_Time)

WebUI.comment(Applicant_Name)

Actions action1 = new Actions(driver)

action1.doubleClick(row).build().perform()

WebUI.delay(5)

//WebUI.doubleClick(findTestObject('Page_AKMS - Loan History/p_Restricted'))
//Verify whehter Loan History Detail page exists
WebUI.verifyElementText(findTestObject('Page_AKMS - Loan History Detail/p_Loan_History_Title'), 'Loan History Detail')

//Verify the details for the Loan History 
// Access Kit Type
WebUI.verifyElementText(findTestObject('Page_AKMS - Loan History Detail/span_Restricted'), Access_Kit_Type)

// Access Kit Name
WebUI.verifyElementText(findTestObject('Page_AKMS - Loan History Detail/span_Rm2301 Office Access Kit'), Access_Kit_Name)

//Borrow Time
WebUI.verifyElementText(findTestObject('Page_AKMS - Loan History Detail/span_11022018 1136'), Borrow_Time)

//Return Time
return_time = WebUI.getText(findTestObject('Page_AKMS - Loan History Detail/span_11022018 1136 ( 1m)'))

if(Return_Time != ''){
	WebUI.verifyMatch(return_time.substring(0, 16), Return_Time, false)
}else{
	WebUI.verifyMatch(return_time, '', false)
}

//Applicant Name
WebUI.verifyElementText(findTestObject('Page_AKMS - Loan History Detail/span_Vic So'), Applicant_Name)

WebUI.click(findTestObject('Page_AKMS - Loan History Detail/button_OK'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('Page_AKMS - Loan History/b_Loan History'), 'Loan History')

