import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), 'AT_AKMS_RcptOperator')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_txtPassword'), '5DOZR2jiUQtL8hICSvtKeFpCpysIl9gK')

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

WebUI.verifyElementAttributeValue(findTestObject('Page_Default/select_KitType_admin'), 'value', 'All', 0)

WebUI.verifyElementAttributeValue(findTestObject('Page_Default/select_KitStatus'), 'value', 'All', 0)

WebUI.verifyElementChecked(findTestObject('Page_Default/radius_Yes_TemporaryStaffCard'), 0)

CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/th_Duration'), 0)

WebUI.delay(5)

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

boolean isAscending = true

for (int row = 0; row < (rows_table.size() - 1); row++) {
    List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    //Get AccessKit D
    WebElement currentDuration = Columns_row.get(4)
	
	String currentDurationStr = currentDuration.getText()
	
	String[] currentDurationStrSplit = currentDurationStr.split(' ')

    //Get AccessKitType
    WebElement currentAccessKitType = Columns_row.get(0)

    Columns_row = rows_table.get(row + 1).findElements(By.tagName('td'))

    WebElement nextDuration = Columns_row.get(4)
	
	String nextDurationStr = nextDuration.getText()

	String[] nextDurationStrSplit = nextDurationStr.split(' ')
	
    WebElement nextAccessKitType = Columns_row.get(0)

    //Check if the table is ordering in ascending sequence according by name.
    if ((currentDurationStr == '') || (nextDurationStr == '')) {
        if (currentDurationStr.compareTo(nextDurationStr) > 0) {
            isAscending = false
        } else if (currentDurationStr.compareTo(nextDurationStr) == 0) {
            if (currentAccessKitType.getText().compareTo(nextAccessKitType.getText()) > 0) {
                isAscending = false
            }
        }
    } else {
		for(int c=0; c<nextDurationStrSplit.length; c++) {
	        if (currentDurationStrSplit[c].compareTo(nextDurationStrSplit[c]) > 0) {
	            isAscending = false
				break
	        } else if (currentDurationStrSplit[c].compareTo(nextDurationStrSplit[c]) == 0) {
	            if (currentAccessKitType.getText().compareTo(nextAccessKitType.getText()) > 0) {
	                isAscending = false
					break
	            }
	        }
		}
    }

    WebUI.verifyMatch(isAscending.toString(), 'true', false)
}

CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Logout'), 0)

WebUI.closeBrowser()

