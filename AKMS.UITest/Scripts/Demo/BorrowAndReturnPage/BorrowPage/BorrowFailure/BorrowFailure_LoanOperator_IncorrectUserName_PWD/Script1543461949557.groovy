import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), 'AT_AKMS_LoanOperator')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_txtPassword'), '2JM/IU8eow8D3UHjE/bMf6KMHWZBGuQJ')

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

for (int i = 0; i < 2; i++) {
    if ((i % 2) == 0) {
        WebUI.selectOptionByValue(findTestObject('Page_Default/select_KitType_normal'), 'Normal', false)
    } else {
        WebUI.selectOptionByValue(findTestObject('Page_Default/select_KitType_normal'), 'Restricted', false)
    }
    
    WebUI.delay(5)

    //get result table
    WebDriver driver = DriverFactory.getWebDriver()

    WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

    List<WebElement> rows_table = table.findElements(By.tagName('tr'))

    Random numGenerator = new Random()

    int rowNum = numGenerator.nextInt(rows_table.size())

    WebElement row = rows_table.get(rowNum)

    List<WebElement> columns = row.findElements(By.tagName('td'))

    while (columns.get(2).getText() != '') {
        rowNum = numGenerator.nextInt(rows_table.size())

        row = rows_table.get(rowNum)

        columns = row.findElements(By.tagName('td'))
    }
    
    Actions action = new Actions(driver)

    action.doubleClick(row).build().perform()

    WebUI.delay(2)

	//incorrect username with correct pwd
    WebUI.setText(findTestObject('Page_AKMS - Borrow Access Kit/input_Login ID'), 'asdsad')

    WebUI.setEncryptedText(findTestObject('Page_AKMS - Borrow Access Kit/input_Password'), 'lk4w9NpQB4K5KE35P26WMg==')
	
	WebUI.click(findTestObject('Page_AKMS - Borrow Access Kit/button_Submit'))
	
	WebUI.verifyAlertPresent(2)

	String firstAlertMsg = WebUI.getAlertText()

	WebUI.verifyMatch(firstAlertMsg, 'Confirm to submit?', false)

	WebUI.acceptAlert()

	WebUI.verifyAlertPresent(2)

	String secondAlertMsg = WebUI.getAlertText()

	WebUI.verifyMatch(secondAlertMsg, 'Invalid username and password', false)

	WebUI.acceptAlert()
    
	//correct user name with incorrect pwd
	WebUI.setText(findTestObject('Page_AKMS - Borrow Access Kit/input_Login ID'), 'samcschan')

    WebUI.setEncryptedText(findTestObject('Page_AKMS - Borrow Access Kit/input_Password'), 'ivCMUeJc0+NZ8XdYLtyehQ==')
	
    WebUI.click(findTestObject('Page_AKMS - Borrow Access Kit/button_Submit'))

    WebUI.verifyAlertPresent(2)

    firstAlertMsg = WebUI.getAlertText()

    WebUI.verifyMatch(firstAlertMsg, 'Confirm to submit?', false)

    WebUI.acceptAlert()

    WebUI.verifyAlertPresent(2)

    secondAlertMsg = WebUI.getAlertText()

    WebUI.verifyMatch(secondAlertMsg, 'Invalid username and password', false)

    WebUI.acceptAlert()
	
	//same as login username and pwd
	WebUI.setText(findTestObject('Page_AKMS - Borrow Access Kit/input_Login ID'), 'AT_AKMS_LoanOperator')
	
	WebUI.setEncryptedText(findTestObject('Page_AKMS - Borrow Access Kit/input_Password'), '2JM/IU8eow8D3UHjE/bMf6KMHWZBGuQJ')
	
	WebUI.click(findTestObject('Page_AKMS - Borrow Access Kit/button_Submit'))

	WebUI.verifyAlertPresent(2)

	firstAlertMsg = WebUI.getAlertText()

	WebUI.verifyMatch(firstAlertMsg, 'Confirm to submit?', false)

	WebUI.acceptAlert()

	WebUI.verifyAlertPresent(2)

	secondAlertMsg = WebUI.getAlertText()

	WebUI.verifyMatch(secondAlertMsg, 'AKMS operator and the borrower must not be the same person', false)

	WebUI.acceptAlert()
	
	//unauthorized account to borrow restricted kit
	if (i == 1) {
		WebUI.setText(findTestObject('Page_AKMS - Borrow Access Kit/input_Login ID'), 'AT_AKMS_Applicant1')

		WebUI.setEncryptedText(findTestObject('Page_AKMS - Borrow Access Kit/input_Password'), 'V1IT8B41+ssLVphK9Ean8kgnKZzUPxq1')

		WebUI.click(findTestObject('Page_AKMS - Borrow Access Kit/button_Submit'))

		WebUI.verifyAlertPresent(2)

		firstAlertMsg = WebUI.getAlertText()

		WebUI.verifyMatch(firstAlertMsg, 'Confirm to submit?', false)

		WebUI.acceptAlert()

		WebUI.verifyAlertPresent(2)

		secondAlertMsg = WebUI.getAlertText()

		WebUI.verifyMatch(secondAlertMsg, 'This applicant is not authorized to borrow this restricted access kit.', false)

		WebUI.acceptAlert()
	}
	
    WebUI.back()
}

WebUI.closeBrowser()

