import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), 'AT_AKMS_LoanOperator')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_txtPassword'), '2JM/IU8eow8D3UHjE/bMf6KMHWZBGuQJ')

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Page_Default/select_KitType_normal'), 'Normal', false)

WebUI.delay(5)

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

Random numGenerator = new Random()

int rowNum = numGenerator.nextInt(rows_table.size())

WebElement row = rows_table.get(rowNum)

List<WebElement> columns = row.findElements(By.tagName('td'))

while (columns.get(2).getText() != '') {
    rowNum = numGenerator.nextInt(rows_table.size())

    row = rows_table.get(rowNum)

    columns = row.findElements(By.tagName('td'))
}

Actions action = new Actions(driver)

action.doubleClick(row).build().perform()

WebUI.delay(2)

WebUI.setText(findTestObject('Page_AKMS - Borrow Access Kit/input_Login ID'), 'samcschan')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Borrow Access Kit/input_Password'), 'lk4w9NpQB4K5KE35P26WMg==')

WebUI.click(findTestObject('Page_AKMS - Borrow Access Kit/button_Submit'))

WebUI.verifyAlertPresent(2)

String firstAlertMsg = WebUI.getAlertText()

WebUI.verifyMatch(firstAlertMsg, 'Confirm to submit?', false)

WebUI.acceptAlert()

WebUI.verifyAlertPresent(2)

String secondAlertMsg = WebUI.getAlertText()

WebUI.verifyMatch(secondAlertMsg, 'Submitted Successfully', false)

WebUI.acceptAlert()

String currentUrl = driver.getCurrentUrl()

WebUI.verifyMatch(currentUrl, 'https://autotestakms.devbss.com/Default.aspx', false)

WebUI.delay(2)

//verify access kit status after borrow
WebUI.selectOptionByValue(findTestObject('Page_Default/select_KitType_normal'), 'Normal', false)

WebUI.delay(3)

table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

rows_table = table.findElements(By.tagName('tr'))

row = rows_table.get(rowNum)

//connect DB
Connection conn = CustomKeywords.'dbConnection.DBConnection.ConnectDB'('UMTLHKSQLA', 'AKMS_AutoTest', '1433')

CallableStatement statement = conn.prepareCall('{Call SelAccessKitBorrwoAndReturnsByTypeAndStatus (?,?,?)}')

statement.setString(1, 'Normal')

statement.setString(2, 'All')

statement.setString(3, 'Normal,Restricted')

ResultSet rs = statement.executeQuery()

for (int i = 0; i <= rowNum; i++) {
    rs.next()
}

columns = row.findElements(By.tagName('td'))

String expectedApplicantName = rs.getString('BorrowedBy')

String tempBorrowedDate = rs.getString('BorrowedDate')

String[] splitedDate = tempBorrowedDate.split('[\' \',\':\']')

String expectedBorrowedDate = ((((((((splitedDate[0].split('-')[1]) + '/') + (splitedDate[0].split('-')[2])) + '/') + (splitedDate[
0].split('-')[0])) + ' ') + (splitedDate[1])) + ':') + (splitedDate[2])

CustomKeywords.'dbConnection.DBConnection.closeDatabaseConnection'()

String actualKitName = columns.get(1).getText()

String actualApplicantName = columns.get(2).getText()

String actualBorrowedDate = columns.get(3).getText()

WebUI.verifyMatch(actualApplicantName, expectedApplicantName, false)

WebUI.verifyMatch(actualBorrowedDate, expectedBorrowedDate, false)

action.doubleClick(row).build().perform()

WebUI.delay(2)

//Return page
//verify kit information in return page
String kitTitle = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Return Access Kit/span_KitTitle'))

WebUI.verifyMatch(actualKitName, kitTitle, false)

WebUI.click(findTestObject('Object Repository/Page_AKMS - Return Access Kit/a_Borrow  Applicant Info'))

WebUI.delay(2)

//verify applicant name in return page
expectedApplicantName = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Return Access Kit/span_applicantName'))

WebUI.verifyMatch(actualApplicantName, expectedApplicantName, false)

WebUI.setEncryptedText(findTestObject('Page_AKMS - Return Access Kit/input_Password'), 'lk4w9NpQB4K5KE35P26WMg==')

WebUI.click(findTestObject('Object Repository/Page_AKMS - Return Access Kit/button_Submit'))

WebUI.verifyAlertPresent(2)

firstAlertMsg = WebUI.getAlertText()

WebUI.verifyMatch(firstAlertMsg, 'Confirm to submit?', false)

WebUI.acceptAlert()

WebUI.verifyAlertPresent(2)

secondAlertMsg = WebUI.getAlertText()

WebUI.verifyMatch(secondAlertMsg, 'Submitted Successfully', false)

WebUI.acceptAlert()

WebUI.verifyMatch(currentUrl, 'https://autotestakms.devbss.com/Default.aspx', false)

//verify the access kit status after return
WebUI.selectOptionByValue(findTestObject('Page_Default/select_KitType_normal'), 'Normal', false)

WebUI.delay(3)

table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

rows_table = table.findElements(By.tagName('tr'))

row = rows_table.get(rowNum)

columns = row.findElements(By.tagName('td'))

//applicant name
WebUI.verifyMatch(columns.get(2).getText(), '', false)

//borrow time
WebUI.verifyMatch(columns.get(3).getText(), '', false)

WebUI.closeBrowser()

