import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import org.junit.After
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), 'AT_AKMS_RcptOperator')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_txtPassword'), '5DOZR2jiUQtL8hICSvtKeFpCpysIl9gK')

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

Random numGenerator = new Random()

int rowNum = numGenerator.nextInt(rows_table.size())

WebElement row = rows_table.get(rowNum)

List<WebElement> columns = row.findElements(By.tagName('td'))

while (columns.get(2).getText() != '') {
    rowNum = numGenerator.nextInt(rows_table.size())

    row = rows_table.get(rowNum)

    columns = row.findElements(By.tagName('td'))
}

String kitType = columns.get(0).getText()

String kitName = columns.get(1).getText()

Actions action = new Actions(driver)

action.doubleClick(row).build().perform()

WebUI.delay(2)

String actualType = WebUI.getText(findTestObject('Page_AKMS - Borrow Access Kit/span_AccessKitType'))

String actualName = WebUI.getText(findTestObject('Page_AKMS - Borrow Access Kit/span_AccessKitName'))

WebUI.verifyMatch(actualType, kitType, false)

WebUI.verifyMatch(actualName, kitName, false)

WebUI.closeBrowser()
