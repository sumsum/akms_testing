import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.awt.event.KeyEvent as KeyEvent
import java.sql.CallableStatement as CallableStatement
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), userName)

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtPassword'), PWD)

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

for (int i = 0; i < 2; i++) {
    if ((i % 2) == 0) {
        WebUI.selectOptionByValue(findTestObject('Page_Default/select_KitType_admin'), 'TSC_Normal', false)
    } else {
        WebUI.selectOptionByValue(findTestObject('Page_Default/select_KitType_admin'), 'TSC_Restricted', false)
    }
    
    WebUI.delay(5)

    //get result table
    WebDriver driver = DriverFactory.getWebDriver()

    WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

    List<WebElement> rows_table = table.findElements(By.tagName('tr'))

    Random numGenerator = new Random()

    int rowNum = numGenerator.nextInt(rows_table.size())

    WebElement row = rows_table.get(rowNum)

    List<WebElement> columns = row.findElements(By.tagName('td'))

    while (columns.get(2).getText() == '') {
        rowNum = numGenerator.nextInt(rows_table.size())

        row = rows_table.get(rowNum)

        columns = row.findElements(By.tagName('td'))
    }
    
    Actions action = new Actions(driver)

    action.doubleClick(row).build().perform()

    WebUI.delay(2)

    WebUI.setText(findTestObject('Object Repository/Page_AKMS - Return Access Kit/input_Password'), 'askmdenf')

    WebUI.sendKeys(findTestObject('Object Repository/Page_AKMS - Return Access Kit/button_Submit'), Keys.chord(Keys.ENTER))

	WebUI.verifyAlertPresent(2)
	
	String firstAlertMsg = WebUI.getAlertText()
	
	WebUI.verifyMatch(firstAlertMsg, 'Confirm to submit?', false)
	
	WebUI.acceptAlert();
	
	WebUI.verifyAlertPresent(2)
	
	String secondAlertMsg = WebUI.getAlertText()
	
	WebUI.verifyMatch(secondAlertMsg, 'Invalid username and password', false)
	
	WebUI.acceptAlert();
	
    WebUI.back()
}

WebUI.closeBrowser()

