import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Default/a_Access Kit Configuration'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/th_Access Kit Type'))

WebUI.delay(1)

//get result table
WebUI.comment('ResultTableSorting_Descending - Access Kit Type')

WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

checkResultTableElementOrder(rows_table, 0)

WebUI.comment('ResultTableSorting_Descending - Access Kit Name')

WebUI.click(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/th_Access Kit Name'))

WebUI.click(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/th_Access Kit Name'))

WebUI.delay(1)

table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

rows_table = table.findElements(By.tagName('tr'))

checkResultTableElementOrder(rows_table, 1)

WebUI.comment('ResultTableSorting_Descending - Status')

WebUI.click(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/th_Status'))

WebUI.click(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/th_Status'))

WebUI.delay(1)

table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

rows_table = table.findElements(By.tagName('tr'))

checkResultTableElementOrder(rows_table, 2)

WebUI.comment('ResultTableSorting_Descending - Included Items')

WebUI.click(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/th_Includes'))

WebUI.click(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/th_Includes'))

WebUI.delay(1)

table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

rows_table = table.findElements(By.tagName('tr'))

checkResultTableElementOrder(rows_table, 3)

//Check if the table is ordering in ascending sequence according by name.
CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Logout'), 0)

WebUI.closeBrowser()

private void checkResultTableElementOrder(List<WebElement> rows_table, int targetColumnIndex) {
    boolean isDescending = true

    for (int row = 0; row < (rows_table.size() - 1); row++) {
		WebUI.comment(row.toString())
		
        List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

        WebElement targetColumn = Columns_row.get(targetColumnIndex)

        Columns_row = rows_table.get(row + 1).findElements(By.tagName('td'))

        WebElement nextTargetColumn = Columns_row.get(targetColumnIndex)

        if (targetColumn.getText().compareToIgnoreCase(nextTargetColumn.getText()) < 0) {
            isDescending = false
        }
		
		WebUI.comment(targetColumn.getText())
		WebUI.comment(nextTargetColumn.getText())
        
        WebUI.verifyMatch(isDescending.toString(), 'true', false)
    }
}

