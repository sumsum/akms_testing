import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Default/a_Access Kit Configuration'))

WebUI.delay(3)

CustomKeywords.'checkFocus.checkElementFocus'(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/input'), 
    'search', 0)

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

//get accesskit borrow and return list from DB
Connection conn = CustomKeywords.'dbConnection.DBConnection.ConnectDB'('UMTLHKSQLA', 'AKMS_AutoTest', '1433')

CallableStatement statement = conn.prepareCall('{Call SelAccessKitConfigurations}')

ResultSet rs = statement.executeQuery()

for (int row = 0; row < rows_table.size(); row++) {
    rs.next()

    List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    for (int column = 0; column < Columns_row.size(); column++) {
        String actualValue = Columns_row.get(column).getText()

        actualValue = actualValue.replace(' ', '')

        String expectedValue = null

        switch (column) {
            case 0:
                expectedValue = rs.getString('AccessKitType')

                break
            case 1:
                expectedValue = rs.getString('AccessKitName')

                break
            case 2:
                String tempExpectedValue = rs.getString('Status')

                expectedValue = tempExpectedValue.replace('On', 'On-')

                break
            case 3:
                String tempExpectedValue = rs.getString('ItemIncludes')

                expectedValue = tempExpectedValue.replace('\r\n ', '\n')

                println(expectedValue)
        }
        
        expectedValue = expectedValue.replace(' ', '')

        WebUI.verifyMatch(actualValue, expectedValue, false)
    }
}

CustomKeywords.'dbConnection.DBConnection.closeDatabaseConnection'()

CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Logout'), 0)

WebUI.closeBrowser()

