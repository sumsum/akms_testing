import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.junit.After as After
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Default/a_Access Kit Configuration'))

WebUI.delay(3)

//Test case - Input Digit
String inputString = '113'

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/input'), inputString)

WebUI.delay(2)

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

checkResultTable(rows_table, inputString, false)

//Test case - Input Chinese Charaters
inputString = '測試字串'

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/input'), inputString)

WebUI.delay(2)

table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

rows_table = table.findElements(By.tagName('tr'))

checkResultTable(rows_table, inputString, false)

//Test case - Input English Characters
inputString = 'Normal'

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/input'), inputString)

WebUI.delay(2)

table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

rows_table = table.findElements(By.tagName('tr'))

checkResultTable(rows_table, inputString, false)

//Test case - Input Special Characters
inputString = '('

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/input'), inputString)

WebUI.delay(2)

table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

rows_table = table.findElements(By.tagName('tr'))

checkResultTable(rows_table, inputString, false)

//Test case - Input Special Characters
inputString = 'ICE'

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/input'), inputString)

WebUI.delay(2)

table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

rows_table = table.findElements(By.tagName('tr'))

checkResultTable(rows_table, inputString, true)

CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Logout'), 0)

WebUI.closeBrowser()

private void checkResultTable(List<WebElement> rows_table, String inputString, boolean isCaseSensitive) {
    boolean isContain = false

    boolean isNoRecord = false

    for (int row = 0; row < rows_table.size(); row++) {
        row.next()

        List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

        for (int column = 0; column < Columns_row.size(); column++) {
            String actualValue = Columns_row.get(column).getText()

            if (actualValue.compareTo('No matching records found') == 0) {
                isNoRecord = true

                break
            }
            
            if (!(isCaseSensitive)) {
                if (actualValue.contains(inputString)) {
                    isContain = true

                    break
                }
            } else {
                if (actualValue.toLowerCase().contains(inputString.toLowerCase())) {
                    isContain = true

                    break
                }
            }
        }
        
        if (isNoRecord) {
            break
        }
        
        WebUI.verifyMatch('true', isContain.toString(), false)
    }
}

