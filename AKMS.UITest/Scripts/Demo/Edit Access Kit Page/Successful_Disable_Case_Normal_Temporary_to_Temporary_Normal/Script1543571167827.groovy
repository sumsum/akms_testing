import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import org.openqa.selenium.interactions.Actions as Actions

//==============================================================================================================
//----------------------------------------------------------Test Case: Access Kit Type changes from Normal/Temporary Staff Card to Temporary Staff Card/ Normal--------------
//WebUI.comment("Test Case: Successful Case: Access Kit Type changes from Normal/Temporary Staff Card to Temporary Staff Card/ Normal Start")
WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_Password_butLogin'))

//Verify login
WebUI.delay(5)

//Go to Access Kit Configuration Page
WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Access Kit Configuration'))

WebUI.delay(10)

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

WebElement row

List<WebElement> columns

for (int a = 0; a < rows_table.size(); a++) {
    row = rows_table.get(a)

    columns = row.findElements(By.tagName('td'))

    if ((columns.get(2).getText() == 'Available') && (columns.get(0).getText() == type)) {
        break
    }
}

Actions action = new Actions(driver)

//Get the Loan History information for specific records
Access_Kit_Type = columns.get(0).getText()

Access_Kit_Name = columns.get(1).getText()

Status = columns.get(2).getText()

Includes = columns.get(3).getText()

Includes = Includes.replace(' ', '')

WebUI.comment(Access_Kit_Type)

WebUI.comment(Access_Kit_Name)

WebUI.comment(Status)

WebUI.comment(Includes)

action.doubleClick(row).build().perform()

WebUI.delay(5)

New_type = 'Normal'

if (type == 'Normal') {
    //-----------------------------------------------------Normal Type----------------------------------------------	
    //Change to Temporary Card Type
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'TSC_Normal', false)

    New_type = 'Temporary Staff Card'

    //------------------------------Verify Fields---------------------------------------------------------------------------------------------------
    //Enable Edit Field
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'TSC_Normal', 0 //-----------------------------------------------------Temporary Staff Card Type-----------------------------------------
        ) //Temporary Card Type
    //------------------------------Verify Fields---------------------------------------------------------------------------------------------------
    //Enable Edit Field
    //Type
} else {
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'Normal', false)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'Normal', 0)
}

//Name
WebUI.verifyElementAttributeValue(findTestObject('Page_AKMS - Add Access Kit/Name (1)'), 'value', Access_Kit_Name, 0)

WebUI.verifyElementClickable(findTestObject('Page_AKMS - Add Access Kit/Name (1)'))

// Notification Parties Fields
WebUI.verifyElementAttributeValue(findTestObject('Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'), 'value', 
    '', 0)

WebUI.verifyElementNotClickable(findTestObject('Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'))

// Notification Parties List
WebUI.verifyElementAttributeValue(findTestObject('Page_AKMS - Add Access Kit/List_Notification _Fields (1)'), 'value', '', 
    0)

WebUI.verifyElementNotClickable(findTestObject('Page_AKMS - Add Access Kit/List_Notification _Fields (1)'))

// Notification Parties Add Button
WebUI.verifyElementNotClickable(findTestObject('Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1)'))

//Notification Parties Delete Button
WebUI.verifyElementNotClickable(findTestObject('Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'), 'value', 
    '', 0)

WebUI.verifyElementNotClickable(findTestObject('Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Page_AKMS - Add Access Kit/List_Authorized_Party (1)'), 'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Page_AKMS - Add Access Kit/List_Authorized_Party (1)'))

// Authorized Party Add Button
WebUI.verifyElementNotClickable(findTestObject('Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1)'))

//Authorized Party Delete Button
WebUI.verifyElementNotClickable(findTestObject('Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1)'))

//Note
WebUI.verifyElementClickable(findTestObject('Page_AKMS - Add Access Kit/Note (1)'))

//Includes
includes1 = WebUI.getText(findTestObject('Page_AKMS - Add Access Kit/Includes (1)')).replace(' ', '')

WebUI.comment(includes1)

WebUI.comment(Includes)

WebUI.verifyMatch(includes1, Includes, false)

WebUI.verifyElementClickable(findTestObject('Page_AKMS - Add Access Kit/Includes (1)'))

//Enable Checkbox
WebUI.verifyElementChecked(findTestObject('Page_AKMS - Edit Access Kit/Button_Enable'), 20)

//Sataus
WebUI.verifyElementText(findTestObject('Object Repository/Page_AKMS - Edit Access Kit/span_On-Loan'), Status)

//Save Button
WebUI.verifyElementClickable(findTestObject('Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

//--------------------------------------------------------------input Edit Field----------------------------------------
New_name = (Access_Kit_Name + '1')

//Name
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'), New_name)

New_Includes = (Includes + '1')

//Includes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), New_Includes)

//Notes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'), '123')

//Enable Checkbox
WebUI.uncheck(findTestObject('Page_AKMS - Edit Access Kit/Button_Enable'))

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

WebUI.delay(5)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Do you confirm to save this Access Kit?', false)

WebUI.acceptAlert()

WebUI.delay(10)

// Verify DB records
//DB Connection
CustomKeywords.'DBConnection.ConnectDB'('UMTLHKSQLA', 'AKMS_AutoTest', '1433')

ResultSet rs = CustomKeywords.'DBConnection.executeQuery'(('select AccessKitId, AccessKitName, AccessKitType, Status,ItemIncludes from dbo.AccessKit where AccessKitName =\'' + 
    New_name) + '\'')

if(rs.next()){

database_name = rs.getString('AccessKitName')

database_type = rs.getString('AccessKitType')

database_status = rs.getString('Status')

database_includes = rs.getString('ItemIncludes')

database_includes = rs.getString('ItemIncludes').replace('\r\n', '\n')

database_includes = database_includes.replace(' ', '')

WebUI.comment(database_name)

WebUI.comment(database_includes)

if (New_type == 'Temporary Staff Card') {
    New_type = 'TSC_Normal'
}

WebUI.verifyMatch(New_type, database_type, false)

WebUI.verifyMatch(New_name, database_name, false)

WebUI.verifyMatch(database_status, 'Disabled', false)

WebUI.verifyMatch(New_Includes, database_includes, false)
}

WebUI.comment('Verify DB record')

WebUI.closeBrowser()

//Verify the "Borrow and Access Kit Page"
WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_Password_butLogin'))

//Verify login
WebUI.delay(5)

//get result table
WebDriver driver1 = DriverFactory.getWebDriver()

WebElement table1 = driver1.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table1 = table1.findElements(By.tagName('tr'))

WebElement row1

List<WebElement> columns1

for (int a = 0; a < rows_table1.size(); a++) {
    row1 = rows_table1.get(a)

    columns1 = row1.findElements(By.tagName('td'))

    if ((columns1.get(1).getText() == New_name) && (columns1.get(0).getText() == New_type)) {
        WebUI.verifyMatch(New_name, New_type, false)

        break
    }
}

WebUI.comment('Test Case: Successful Case: Access Kit Type changes from Normal/Temporary Staff Card to Temporary Staff Card/ Normal End')

CustomKeywords.'DBConnection.closeDatabaseConnection'()

WebUI.closeBrowser()

