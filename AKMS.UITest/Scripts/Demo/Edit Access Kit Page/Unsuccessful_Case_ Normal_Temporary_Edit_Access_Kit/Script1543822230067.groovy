import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import org.openqa.selenium.interactions.Actions as Actions

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_Password_butLogin'))

//Verify login
WebUI.delay(5)

//Go to Access Kit Configuration Page
WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Access Kit Configuration'))

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

WebElement row = null

List<WebElement> columns = null

for (int a = 0; a < rows_table.size(); a++) {
    row = rows_table.get(a)

    columns = row.findElements(By.tagName('td'))

    if ((columns.get(2).getText() == 'Available') && (columns.get(0).getText() == type)) {
        break
    }
}

Actions action = new Actions(driver)

//Get the Loan History information for specific records
Access_Kit_Type = columns.get(0).getText()

Access_Kit_Name = columns.get(1).getText()

Status = columns.get(2).getText()

Includes = columns.get(3).getText()

Includes = Includes.replace(' ', '')

WebUI.comment(Access_Kit_Type)

WebUI.comment(Access_Kit_Name)

WebUI.comment(Status)

WebUI.comment(Includes)

action.doubleClick(row).build().perform()

WebUI.delay(10)

if (type == 'Normal') {
    //-----------------------------------------------------UnSuccessful Case : Normal Type-----------------------------------------
    //------------------------------Verify Fields---------------------------------------------------------------------------------------------------
    //Enable Edit Field
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'Normal', 0 //------------------------------Verify Fields---------------------------------------------------------------------------------------------------
        //Enable Edit Field
        ) //Type
} else {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'TSC_Normal', 0)
}

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'))

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'), 'value', Access_Kit_Name, 
    0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'))

//Note
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'))

//Includes
String UI_includes = WebUI.getAttribute(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), 'value')

UI_includes = UI_includes.replace(' ', '')

WebUI.verifyMatch(UI_includes, Includes, false)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'))

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

//Disable Edit Field
// Notification Parties Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'))

// Notification Parties List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'))

// Notification Parties Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1)'))

//Notification Parties Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'))

// Authorized Party Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1)'))

//Authorized Party Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1)'))

//--------------------------------------------------------Input something in Edit Fields-------------------------------------------------------
//==================================================================================================================================================================
//-------------------------------------------------------Test Case: Don't input all Text Fields --------------------------------------------------------
WebUI.comment('Test Case: Don\'t input any text fields Start')

//Name
WebUI.clearText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'))

//Notes
WebUI.clearText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'))

//Includes
WebUI.clearText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'))

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

WebUI.delay(5)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Please input Access Kit Name', false)

WebUI.acceptAlert()

WebUI.delay(5)

//------------------------------------------Verify Fields---------------------------------------------------------------------------------------------------
//Enable Edit Field
if (type == 'Normal') {
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'Normal', 0 //Type
        )
} else {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'TSC_Normal', 0)
}

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'), 'value', '', 
    0)

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'), 'value', '', 
    0)

//Includes
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), 'value', 
    '', 0)

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

//Disable Edit Field
// Notification Parties Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'))

// Notification Parties List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'))

// Notification Parties Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1)'))

//Notification Parties Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'))

WebUI.comment('Test Case: Don\'t input any text fields End')

//--------------------------------------------------------------Test Case: Don't input any text fields End---------------------------------------------------------
//================================================================================================================================================================
//-------------------------------------------------------Test Case: Don't input Access Kit Name--------------------------------------------------------
WebUI.comment('Test Case: Don\'t input Access Kit Name Start')

//Includes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), includes)

//Notes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'), notes)

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

WebUI.delay(5)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Please input Access Kit Name', false)

WebUI.acceptAlert()

WebUI.delay(5)

//------------------------------------------Verify Fields---------------------------------------------------------------------------------------------------
//Enable Edit Field
if (type == 'Normal') {
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'Normal', 0 //Type
        )
} else {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'TSC_Normal', 0)
}

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'), 'value', '', 
    0)

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'), 'value', notes, 
    0)

//Includes
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), 'value', 
    includes, 0)

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

//Disable Edit Field
// Notification Parties Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'))

// Notification Parties List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'))

// Notification Parties Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1)'))

//Notification Parties Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'))

WebUI.comment('Test Case: Don\'t input Access Kit Name End')

//--------------------------------------------------------------Test Case: Don't input Access Kit Name End----------------------------------------------------------------------------
//==================================================================================================================================================================
//-------------------------------------------------------Test Case: Don't input includes --------------------------------------------------------
WebUI.comment('Test Case: Don\'t input includes Start')

//Name
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'), name)

//Includes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), ' ')

//WebUI.clearText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'))
//Notes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'), notes)

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

WebUI.delay(5)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Do you confirm to save this Access Kit?', false)

WebUI.acceptAlert()

WebUI.delay(5)

//--------------------------------------------------------------Test Case: Don't input Includes End---------------------------------------------------------
//======================================================================================================================
//--------------------------------------------------------------Test Case: Use same Access Kit name with existing record---------------------------------------------------------
WebUI.comment('Test Case: Use same Access Kit name with existing record Start')

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_Password_butLogin'))

//Verify login
WebUI.delay(5)

//Go to Access Kit Configuration Page
WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Access Kit Configuration'))

//Click "Add Access Kit" Button
WebUI.click(findTestObject('Page_AKMS - Access Kit Configuratio/input_Access Kit Configuration'))

WebUI.delay(5)

WebUI.comment(type)

if (type == 'Normal') {
    //-----------------------------------------------------UnSuccessful Case : Normal Type-----------------------------------------
    //Temporary Card Type
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'TSC_Normal', false)

    //Change to Normal Type
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'Normal', false)

    //------------------------------Verify Fields---------------------------------------------------------------------------------------------------
    //Enable Edit Field
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'Normal', 0 //-----------------------------------------------------UnSuccessful Case : Temporary Card Type-----------------------------------------
        //Temporary Card Type
        ) //------------------------------Verify Fields---------------------------------------------------------------------------------------------------
    //Enable Edit Field
    //Type
} else {
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'TSC_Normal', false)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'TSC_Normal', 0)
}

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'))

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'), 'value', '', 
    0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'))

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'), 'value', '', 
    0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'))

//Includes
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'))

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

//Disable Edit Field
// Notification Parties Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'))

// Notification Parties List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'))

// Notification Parties Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1)'))

//Notification Parties Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'))

// Authorized Party Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1)'))

//Authorized Party Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1)'))

//--------------------------------------------------------Input something in Edit Fields-------------------------------------------------------
// Verify DB records  
//DB Connection
CustomKeywords.'DBConnection.ConnectDB'('UMTLHKSQLA', 'AKMS_AutoTest', '1433')

ResultSet rs = CustomKeywords.'DBConnection.executeQuery'('select TOP(1) AccessKitId, AccessKitName, AccessKitType, Status,ItemIncludes from dbo.AccessKit order by AccessKitId desc')

rs.next()

database_name = rs.getString('AccessKitName')

//Name
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'), database_name)

//Includes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), includes)

//Notes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'), notes)

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

WebUI.delay(10)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Do you confirm to save this Access Kit?', false)

WebUI.acceptAlert()

WebUI.delay(5)

WebUI.comment('Test Case: Use same Access Kit name with existing record End')

