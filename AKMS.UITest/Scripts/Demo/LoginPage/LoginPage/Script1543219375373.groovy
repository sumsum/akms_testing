import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), variable)

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtPassword'), variable_0)

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

if (variable_1 == 'Successful Login') {
    WebUI.verifyElementNotPresent(findTestObject('Page_AKMS - Login/div_Invalid username and passw'), 
        5)

    WebUI.delay(2)

    WebUI.waitForElementPresent(findTestObject('Page_AKMS - Borrow  Return/b_Borrow  Return'), 5)

    WebUI.verifyElementText(findTestObject('Page_AKMS - Borrow  Return/b_Borrow  Return'), 'Borrow & Return') //WebUI.openBrowser()
} else if (variable_1 == 'Block Account') {
    WebUI.verifyElementText(findTestObject('Page_AKMS - Login/div_Invalid username and passw'), 
        'AD server is not available')
} else if (variable_1 == 'No Permission') {
    WebUI.verifyElementText(findTestObject('Page_AKMS - Login/div_Invalid username and passw'), 
        'You do not have permission to login')
} else if (variable_1 == 'Normal User: Wrong Password') {
    for (int a = 0; a <= 2; a++) {
        WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

        WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), variable)

        WebUI.setText(findTestObject('Page_AKMS - Login/input_txtPassword'), variable_0)

        WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

        WebUI.delay(2)

        WebUI.verifyElementText(findTestObject('Page_AKMS - Login/div_Invalid username and passw'), 
            'Invalid username and password')
    }
    
    WebUI.verifyElementText(findTestObject('Page_AKMS - Login/div_Invalid username and passw'), 
        'Invalid username and password')
} else {
    WebUI.verifyElementText(findTestObject('Page_AKMS - Login/div_Invalid username and passw'), 
        'Invalid username and password')
}

WebUI.delay(5)

WebUI.closeBrowser()

