import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.time.LocalDateTime as LocalDateTime
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), userName)

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtPassword'), PWD)

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_Default/a_Loan History'))

WebUI.delay(5)

//Get to date
String dbToDateStr = '10/01/2018'

//Get from date
String fromDateStr = '11/01/2018'

//SearchFailure - To Date is earlier than From Date
WebUI.comment('SearchFailure - To Date is earlier than From Date')

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), '10/31/2018')

WebUI.sendKeys(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), '10/01/2018')

WebUI.sendKeys(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/button_Apply Filter'))

WebUI.verifyAlertPresent(2)

String alertMsg = WebUI.getAlertText()

WebUI.verifyMatch(alertMsg, 'From Date cannot be earlier than To Date', false)

WebUI.acceptAlert()

WebUI.refresh()

WebUI.delay(5)

//SearchFailure - From Date is empty
WebUI.comment('SearchFailure - From Date is empty')

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), '')

WebUI.sendKeys(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/button_Apply Filter'))

WebUI.verifyAlertPresent(2)

alertMsg = WebUI.getAlertText()

WebUI.verifyMatch(alertMsg, 'Please input Borrow Date (From)', false)

WebUI.acceptAlert()

WebUI.refresh()

WebUI.delay(5)

//SearchFailure - To Date is empty
WebUI.comment('SearchFailure - To Date is empty')

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), '')

WebUI.sendKeys(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/button_Apply Filter'))

WebUI.verifyAlertPresent(2)

alertMsg = WebUI.getAlertText()

WebUI.verifyMatch(alertMsg, 'Please input Borrow Date (To)', false)

WebUI.acceptAlert()

CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Logout'), 0)

WebUI.closeBrowser()

