import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.time.LocalDateTime as LocalDateTime

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), userName)

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtPassword'), PWD)

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_Default/a_Loan History'))

WebUI.delay(2)

CustomKeywords.'checkFocus.checkElementFocus'(findTestObject('Object Repository/Page_AKMS - Loan History/input'), 'search', 
    0)

if (userName.compareTo('AT_AKMS_LoanOperator') == 0) {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/select_KitType_normal'), 
        'value', 'All', 0)
} else {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/select_KitType_admin'), 
        'value', 'All', 0)
}

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input_Access Kit Name_txtAcces'), 
    'value', '', 0)

//set date format in string
DateTimeFormatter dtf = DateTimeFormatter.ofPattern('MM/dd/yyyy')

//Get to date
LocalDateTime toDate = LocalDateTime.now()

String toDateStr = dtf.format(toDate)

LocalDateTime dbToDate = toDate.plusDays(1)

String dbToDateStr = dtf.format(dbToDate)

//Get from date
LocalDateTime fromDate = toDate.minusDays(30)

String fromDateStr = dtf.format(fromDate)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), 'value', 
    fromDateStr, 0)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), 'value', 
    toDateStr, 0)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input_Applicant Name_txtApplic'), 
    'value', '', 0)

isPresent = WebUI.verifyElementPresent(findTestObject('Object Repository/Page_AKMS - Loan History/input'), 2, FailureHandling.OPTIONAL)

if (isPresent) {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input'), 'value', '', 0)

    WebUI.delay(2)

    WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Borrow Time'))
}

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

//get accesskit borrow and return list from DB
Connection conn = CustomKeywords.'dbConnection.DBConnection.ConnectDB'('UMTLHKSQLA', 'AKMS_AutoTest', '1433')

CallableStatement statement = conn.prepareCall('{Call SelAccessKitLoanedHistoryByCriteria (?,?,?,?,?,?)}', ResultSet.TYPE_SCROLL_INSENSITIVE, 
    ResultSet.CONCUR_READ_ONLY)

statement.setString(1, 'All')

statement.setString(2, '')

statement.setString(3, '')

statement.setString(4, fromDateStr)

statement.setString(5, dbToDateStr)

if (userName.compareTo('AT_AKMS_LoanOperator') == 0) {
    statement.setString(6, 'Normal,Restricted')
} else {
    statement.setString(6, 'Normal,Restricted,TSC_Normal,TSC_Restricted')
}

ResultSet rs = statement.executeQuery()

int count = 0

rs.last()

if (rows_table.size() <= 0) //check the result table exist or not(if the table does not have any row that means it does not exit and no record was found)
{
    WebUI.verifyMatch(WebUI.getText(findTestObject('Page_AKMS - Loan History/b_No Record Found')), 'No Record Found', false)
} else {
    for (int row = 0; row < rows_table.size(); row++) {
        List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

        for (int column = 0; column < Columns_row.size(); column++) {
            String actualValue = Columns_row.get(column).getText()

            String expectedValue = null

            switch (column) {
                case 0:
                    expectedValue = rs.getString('AccessKitType')

                    break
                case 1:
                    expectedValue = rs.getString('AccessKitName')

                    break
                case 2:
                    String tempDate = rs.getString('BorrowedDate')

                    if (!(tempDate.equals(null))) {
                        String[] splitedDate = tempDate.split('[\' \',\':\']')

                        expectedValue = (((((((((splitedDate[0].split('-')[1]) + '/') + (splitedDate[0].split('-')[2])) + 
                        '/') + (splitedDate[0].split('-')[0])) + ' ') + (splitedDate[1])) + ':') + (splitedDate[2]))
                    } else {
                        expectedValue = ''
                    }
                    
                    break
                case 3:
                    String tempDate = rs.getString('ReturnedDate')

                    if (!(tempDate.equals(null))) {
                        String[] splitedDate = tempDate.split('[\' \',\':\']')

                        expectedValue = (((((((((splitedDate[0].split('-')[1]) + '/') + (splitedDate[0].split('-')[2])) + 
                        '/') + (splitedDate[0].split('-')[0])) + ' ') + (splitedDate[1])) + ':') + (splitedDate[2]))
                    } else {
                        expectedValue = ''
                    }
                    
                    break
                case 5:
                    expectedValue = rs.getString('ApplicantName')
            }
            
            if (!(expectedValue.equals(null))) {
                WebUI.verifyMatch(actualValue, expectedValue, false)
            }
        }
        
        rs.previous()
    }
}

CustomKeywords.'dbConnection.DBConnection.closeDatabaseConnection'()

CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Logout'), 0)

WebUI.closeBrowser()

