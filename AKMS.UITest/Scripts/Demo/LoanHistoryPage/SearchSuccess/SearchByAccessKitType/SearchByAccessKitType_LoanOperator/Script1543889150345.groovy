import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.time.LocalDateTime as LocalDateTime

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), 'AT_AKMS_LoanOperator')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_txtPassword'), '2JM/IU8eow8D3UHjE/bMf6KMHWZBGuQJ')

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_Default/a_Loan History'))

WebUI.delay(5)

String kitType = 'Normal'

for (int i = 0; i < 2; i++) {
    if (i != 0) {
        kitType = 'Restricted'
    }
    
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Loan History/select_KitType_normal'), kitType, 
        false)

    WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/button_Apply Filter'))

    WebUI.delay(5)

    //set date format in string
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern('MM/dd/yyyy')

    //Get to date
    LocalDateTime toDate = LocalDateTime.now()

    String toDateStr = dtf.format(toDate)

    LocalDateTime dbToDate = toDate.plusDays(1)

    String dbToDateStr = dtf.format(dbToDate)

    //Get from date
    LocalDateTime fromDate = toDate.minusDays(30)

    String fromDateStr = dtf.format(fromDate)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), 'value', 
        fromDateStr, 0)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), 'value', 
        toDateStr, 0)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input_Applicant Name_txtApplic'), 
        'value', '', 0)
	
	WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input'), 'value', '', 0)
	
	//get result table
	WebDriver driver = DriverFactory.getWebDriver()

	WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

	List<WebElement> rows_table = table.findElements(By.tagName('tr'))
	
	WebUI.comment(rows_table.size().toString())
	
	if(rows_table.size() != 0){

		WebUI.delay(5)
		
	    WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Borrow Time'))
	
		driver = DriverFactory.getWebDriver()
		
		table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))
		
		rows_table = table.findElements(By.tagName('tr'))
	
	    //get accesskit borrow and return list from DB
	    Connection conn = CustomKeywords.'dbConnection.DBConnection.ConnectDB'('UMTLHKSQLA', 'AKMS_AutoTest', '1433')
	
	    CallableStatement statement = conn.prepareCall('{Call SelAccessKitLoanedHistoryByCriteria (?,?,?,?,?,?)}', ResultSet.TYPE_SCROLL_INSENSITIVE, 
	        ResultSet.CONCUR_READ_ONLY)
	
	    statement.setString(1, kitType)
	
	    statement.setString(2, '')
	
	    statement.setString(3, '')
	
	    statement.setString(4, fromDateStr)
	
	    statement.setString(5, dbToDateStr)
	
	    statement.setString(6, 'Normal,Restricted')
	
	    ResultSet rs = statement.executeQuery()
	
	    int count = 0
	
	    rs.last()
	
	    for (int row = 0; row < rows_table.size(); row++) {
	        List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))
	
	        for (int column = 0; column < Columns_row.size(); column++) {
	            String actualValue = Columns_row.get(column).getText()
	
	            String expectedValue = null
	
	            switch (column) {
	                case 0:
	                    expectedValue = rs.getString('AccessKitType')
	                    break
	                case 1:
	                    expectedValue = rs.getString('AccessKitName')
	                    break
	                case 2:
	                    String tempDate = rs.getString('BorrowedDate')
	
	                    if (!(tempDate.equals(null))) {
	                        String[] splitedDate = tempDate.split('[\' \',\':\']')
	
	                        expectedValue = (((((((((splitedDate[0].split('-')[1]) + '/') + (splitedDate[0].split('-')[2])) + 
	                        '/') + (splitedDate[0].split('-')[0])) + ' ') + (splitedDate[1])) + ':') + (splitedDate[2]))
	                    } else {
	                        expectedValue = ''
	                    }
	                    
	                    break
	                case 3:
	                    String tempDate = rs.getString('ReturnedDate')
	
	                    if (!(tempDate.equals(null))) {
	                        String[] splitedDate = tempDate.split('[\' \',\':\']')
	
	                        expectedValue = (((((((((splitedDate[0].split('-')[1]) + '/') + (splitedDate[0].split('-')[2])) + 
	                        '/') + (splitedDate[0].split('-')[0])) + ' ') + (splitedDate[1])) + ':') + (splitedDate[2]))
	                    } else {
	                        expectedValue = ''
	                    }
	                    
	                    break
	                case 5:
	                    expectedValue = rs.getString('ApplicantName')
	            }
	            
	            if (!(expectedValue.equals(null))) {
	                WebUI.verifyMatch(actualValue, expectedValue, false)
	            }
	        }
	        
	        rs.previous()
	    }
	    
	    CustomKeywords.'dbConnection.DBConnection.closeDatabaseConnection'()
	}else{
		WebUI.verifyElementText(findTestObject('Object Repository/Page_AKMS - Loan History/b_No Record Found'), 'No Record Found')
	}
}

CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Logout'), 0)

WebUI.closeBrowser()

