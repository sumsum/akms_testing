import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.time.LocalDateTime as LocalDateTime
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), userName)

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtPassword'), PWD)

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_Default/a_Loan History'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), '10/01/2018')

WebUI.sendKeys(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), '10/31/2018')

WebUI.sendKeys(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/button_Apply Filter'))

WebUI.delay(2)

//set date format in string
DateTimeFormatter dtf = DateTimeFormatter.ofPattern('MM/dd/yyyy')

//Get to date
String dbToDateStr = '11/01/2018'

//Get from date
String fromDateStr = '10/01/2018'

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/select_KitType_admin'), 'value', 
    'All', 0)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input_Applicant Name_txtApplic'), 
    'value', '', 0)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input'), 'value', '', 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Borrow Time'))

//get result table
WebDriver driver = DriverFactory.getWebDriver()

WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))

List<WebElement> rows_table = table.findElements(By.tagName('tr'))

//get accesskit borrow and return list from DB
Connection conn = CustomKeywords.'dbConnection.DBConnection.ConnectDB'('UMTLHKSQLA', 'AKMS_AutoTest', '1433')

CallableStatement statement = conn.prepareCall('{Call SelAccessKitLoanedHistoryByCriteria (?,?,?,?,?,?)}', ResultSet.TYPE_SCROLL_INSENSITIVE, 
    ResultSet.CONCUR_READ_ONLY)

statement.setString(1, 'All')

statement.setString(2, '')

statement.setString(3, '')

statement.setString(4, fromDateStr)

statement.setString(5, dbToDateStr)

if (userName.compareTo('AT_AKMS_LoanOperator') == 0) {
    statement.setString(6, 'Normal,Restricted')
} else {
    statement.setString(6, 'Normal,Restricted,TSC_Normal,TSC_Restricted')
}

ResultSet rs = statement.executeQuery()

int count = 0

rs.last()

for (int row = 0; row < rows_table.size(); row++) {
    List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    for (int column = 0; column < Columns_row.size(); column++) {
        String actualValue = Columns_row.get(column).getText()

        String expectedValue = null

        switch (column) {
            case 0:
                expectedValue = rs.getString('AccessKitType')

                break
            case 1:
                expectedValue = rs.getString('AccessKitName')

                break
            case 2:
                String tempDate = rs.getString('BorrowedDate')

                if (!(tempDate.equals(null))) {
                    String[] splitedDate = tempDate.split('[\' \',\':\']')

                    expectedValue = (((((((((splitedDate[0].split('-')[1]) + '/') + (splitedDate[0].split('-')[2])) + '/') + 
                    (splitedDate[0].split('-')[0])) + ' ') + (splitedDate[1])) + ':') + (splitedDate[2]))
                } else {
                    expectedValue = ''
                }
                
                break
            case 3:
                String tempDate = rs.getString('ReturnedDate')

                if (!(tempDate.equals(null))) {
                    String[] splitedDate = tempDate.split('[\' \',\':\']')

                    expectedValue = (((((((((splitedDate[0].split('-')[1]) + '/') + (splitedDate[0].split('-')[2])) + '/') + 
                    (splitedDate[0].split('-')[0])) + ' ') + (splitedDate[1])) + ':') + (splitedDate[2]))
                } else {
                    expectedValue = ''
                }
                
                break
            case 5:
                expectedValue = rs.getString('ApplicantName')
        }
        
        if (!(expectedValue.equals(null))) {
            WebUI.verifyMatch(actualValue, expectedValue, false)
        }
    }
    
    rs.previous()
}

CustomKeywords.'dbConnection.DBConnection.closeDatabaseConnection'()

CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Loan History'), 0)

WebUI.closeBrowser()

