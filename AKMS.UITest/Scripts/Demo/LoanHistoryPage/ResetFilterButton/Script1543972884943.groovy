import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.sql.ResultSet as ResultSet
import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.time.LocalDateTime as LocalDateTime
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), userName)

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtPassword'), PWD)

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_Default/a_Loan History'))

WebUI.delay(2)

if (userName.compareTo('AT_AKMS_LoanOperator') != 0) {
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Loan History/select_KitType_admin'), 'Normal', 
        false)
} else {
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Loan History/select_KitType_normal'), 'Normal', 
        false)
}

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input_Access Kit Name_txtAcces'), 'Rm1011 Staging Room Access Kit')

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), '10/01/2018')

WebUI.sendKeys(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), Keys.chord(Keys.ENTER))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), '10/31/2018')

WebUI.sendKeys(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), Keys.chord(Keys.ENTER))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Page_AKMS - Loan History/input_Applicant Name_txtApplic'), 'ATAK_Testing')

WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/button_Reset Filter'))

WebUI.delay(2)

//set date format in string
DateTimeFormatter dtf = DateTimeFormatter.ofPattern('MM/dd/yyyy')

//Get to date
LocalDateTime toDate = LocalDateTime.now()

String toDateStr = dtf.format(toDate)

//Get from date
LocalDateTime fromDate = toDate.minusDays(30)

String fromDateStr = dtf.format(fromDate)

if (userName.compareTo('AT_AKMS_LoanOperator') != 0) {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/select_KitType_admin'), 
        'value', 'All', 0)
} else {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/select_KitType_normal'), 
        'value', 'All', 0)
}

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input_Access Kit Name_txtAcces'), 
    'value', '', 0)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtFromDate'), 'value', 
    fromDateStr, 0)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input__txtToDate'), 'value', 
    toDateStr, 0)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Loan History/input_Applicant Name_txtApplic'), 
    'value', '', 0)

CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Logout'), 0)

WebUI.closeBrowser()

