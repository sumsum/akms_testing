import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.sql.ResultSet as ResultSet
import java.util.List

import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.time.LocalDateTime as LocalDateTime

import java.sql.Connection as Connection
import java.sql.CallableStatement as CallableStatement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com')

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtUserName'), userName)

WebUI.setText(findTestObject('Page_AKMS - Login/input_txtPassword'), PWD)

WebUI.click(findTestObject('Page_AKMS - Login/input_butLogin'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_Default/a_Loan History'))

WebUI.delay(3)

//get result table
WebUI.comment('ResultTableSorting_Descending - Access Kit Type')

isPresent = WebUI.verifyElementPresent(findTestObject('Object Repository/Page_AKMS - Loan History/input'), 2, FailureHandling.OPTIONAL)

if (isPresent) {
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Access Kit Type'))
	
	WebDriver driver = DriverFactory.getWebDriver()
	
	WebElement table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))
	
	List<WebElement> rows_table = table.findElements(By.tagName('tr'))
	
	checkResultTableElementOrder(rows_table, 0, false)
	
	WebUI.comment('ResultTableSorting_Descending - Access Kit Name')
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Access Kit Name'))
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Access Kit Name'))
	
	WebUI.delay(1)
	
	table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))
	
	rows_table = table.findElements(By.tagName('tr'))
	
	checkResultTableElementOrder(rows_table, 1, false)
	
	WebUI.comment('ResultTableSorting_Descending - Borrowed Time')
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Borrow Time'))
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Borrow Time'))
	
	WebUI.delay(1)
	
	table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))
	
	rows_table = table.findElements(By.tagName('tr'))
	
	checkResultTableElementOrder(rows_table, 2, true)
	
	WebUI.comment('ResultTableSorting_Descending - Return Time')
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Return Time'))
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Return Time'))
	
	WebUI.delay(1)
	
	table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))
	
	rows_table = table.findElements(By.tagName('tr'))
	
	checkResultTableElementOrder(rows_table, 3, true)
	
	WebUI.comment('ResultTableSorting_Descending - Duration')
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Duration'))
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Duration'))
	
	WebUI.delay(1)
	
	table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))
	
	rows_table = table.findElements(By.tagName('tr'))
	
	checkResultTableElementOrder(rows_table, 4, false)
	
	WebUI.comment('ResultTableSorting_Descending - Applicant Name')
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Applicant Name'))
	
	WebUI.click(findTestObject('Object Repository/Page_AKMS - Loan History/th_Applicant Name'))
	
	WebUI.delay(1)
	
	table = driver.findElement(By.xpath('//*[@id="ResultTable"]/tbody'))
	
	rows_table = table.findElements(By.tagName('tr'))
	
	checkResultTableElementOrder(rows_table, 5, false)
	//Check if the table is ordering in Descending sequence according by name.
}


CustomKeywords.'clickCustom.clickJS.clickUsingJS'(findTestObject('Page_Default/a_Logout'), 0)

WebUI.closeBrowser()

private void checkResultTableElementOrder(List<WebElement> rows_table, int targetColumnIndex, boolean isDate){
	boolean isDescending = true
	
	if(!isDate){
		for (int row = 0; row < (rows_table.size() - 1); row++) {
			List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))
		
			WebElement targetColumn = Columns_row.get(targetColumnIndex)
			
			String targetText = targetColumn.getText()
		
			Columns_row = rows_table.get(row + 1).findElements(By.tagName('td'))
		
			WebElement nextTargetColumn = Columns_row.get(targetColumnIndex)
			
			String nextTargetText = nextTargetColumn.getText()
			
			WebUI.comment(targetText + ", " + nextTargetText)
			
			if(stringComparing(targetText, nextTargetText) < 0){
				isDescending = false;
			}
		}
	}else{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm")
	
		for (int row = 0; row < (rows_table.size() - 1); row++) {
			List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))
		
			WebElement targetColumn = Columns_row.get(targetColumnIndex)
			
			String targetText = targetColumn.getText()
			
			LocalDateTime targetDate = null
			
			if(targetText != "")
				targetDate = LocalDateTime.parse(targetText, formatter)
		
			Columns_row = rows_table.get(row + 1).findElements(By.tagName('td'))
		
			WebElement nextTargetColumn = Columns_row.get(targetColumnIndex)
			
			String nextTargetText = nextTargetColumn.getText()
			
			LocalDateTime nextTargetDate = null
			
			if(nextTargetText != "")
				nextTargetDate = LocalDateTime.parse(nextTargetText, formatter)
			
			if(targetDate == null || nextTargetDate == null){
				if(stringComparing(targetText, nextTargetText) < 0){
					isDescending = false;
				}
			}else{
				if(targetDate.compareTo(nextTargetDate) < 0){
					isDescending = false;
				}
			}
		}
	}
	
	WebUI.verifyMatch(isDescending.toString(), 'true', false)
}

private int stringComparing(String str1, String str2){
	//result = 0(equal), > 0(str1 > str2), < 0(str1 < str2)
	int result = 0
	
	int strLength = str1.length()
	
	if(str1.length() > str2.length())
		strLength = str2.length()
	else
		strLength = str1.length()

	for(int i=0; i < strLength; i++){
		if(str1.charAt(i).compareTo(str2.charAt(i)) > 0){
			result = 1;
			break;
		}else if (str1.charAt(i).compareTo(str2.charAt(i)) < 0){
			result = -1;
			break;
		}
	}

	if(str1.contains("<") && !str2.contains("<") && str2 != ""){
		result = -1;
	}
	
	return result;
}

