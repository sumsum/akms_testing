import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.junit.experimental.categories.IncludeCategories.IncludesAny as IncludesAny
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import java.sql.ResultSet as ResultSet

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_Password_butLogin'))

//Verify login
WebUI.delay(5)

//Go to Access Kit Configuration Page
WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Access Kit Configuration'))

WebUI.delay(5)

//WebUI.click(findTestObject('Page_AKMS - Access Kit Configuratio/b_Access Kit Configuration'))
//Click "Add Access Kit" Button
WebUI.click(findTestObject('Page_AKMS - Access Kit Configuratio/input_Access Kit Configuration'))

WebUI.delay(5)

if (type == 'Restricted') {
    //-----------------------------------------------------UnSuccessful Case : Restricted Type-----------------------------------------
    //Temporary Card Type
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'Restricted', false)

    //-----------------------------------------------------Verify Fields----------------------------------------------------------------
    //Enable Edit Field
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'Restricted', 0)

    // Notification Parties Fields
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    // Notification Parties List
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    // Notification Parties Add Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    //Notification Parties Delete Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)' //-----------------------------------------------------UnSuccessful Case : Temporary Staff Card Restricted Type-----------------------------------------
            ) //Temporary Card Type
        ) //-----------------------------------------------------Verify Fields----------------------------------------------------------------
    //Enable Edit Field
    //Type
    // Notification Parties Fields
    // Notification Parties List
    // Notification Parties Add Button
    //Notification Parties Delete Button
} else {
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'TSC_Restricted', false)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'TSC_Restricted', 0)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)'))
}

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'))

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'))

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'))

//Includes
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'))

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1) (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'), 
    'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1) (1)'), 
    'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1) (1)'))

// Authorized Party Add Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1) (1)'))

//Authorized Party Delete Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1) (1)'))

//--------------------------------------------------------Input something in Edit Fields-------------------------------------------------------
//==================================================================================================================================================================
//-------------------------------------------------------Test Case: Don't input all Text Fields --------------------------------------------------------
WebUI.comment('Test Case: Don\'t input any text fields Start')

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

WebUI.delay(5)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Please input Authorized Parties', false)

WebUI.acceptAlert()

WebUI.delay(5)

//------------------------------------------Verify Fields---------------------------------------------------------------------------------------------------
if (type == 'Restricted') {
    //-----------------------------------------------------Verify Fields----------------------------------------------------------------
    //Enable Edit Field
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'Restricted', 0)

    // Notification Parties Fields
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    // Notification Parties List
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    // Notification Parties Add Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    //Notification Parties Delete Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)' //-----------------------------------------------------Verify Fields----------------------------------------------------------------
            ) //Enable Edit Field
        ) //Type
    // Notification Parties Fields
    // Notification Parties List
    // Notification Parties Add Button
    //Notification Parties Delete Button
} else {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'TSC_Restricted', 0)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)'))
}

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'))

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'))

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'))

//Includes
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'))

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1) (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'), 
    'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1) (1)'), 
    'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1) (1)'))

// Authorized Party Add Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1) (1)'))

//Authorized Party Delete Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1) (1)'))

WebUI.comment('Test Case: Don\'t input any text fields End')

//--------------------------------------------------------------Test Case: Don't input any text fields End---------------------------------------------------------
//================================================================================================================================================================
//-------------------------------------------------------Test Case: Don't input Access Kit Name --------------------------------------------------------
WebUI.comment('Test Case: Don\'t input Access Kit Name')

//--------------------------------------------------------Input something in Edit Fields-------------------------------------------------------
/*//Name
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'), name)*/
if (type == 'Restricted') {
    //Notification Parties
    WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        notification_party_email_1)

    WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    WebUI.delay(5)

    Notification_Field = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select__2def41devbss.com'))

    WebUI.verifyMatch(Notification_Field, notification_party_email_1, false)
}

//Authorized Parties
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'), authorized_party_prefix1)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Suggestion_List (1)'), 5)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/suggestion _value_rayhuen (1)'))

// Authorized Party Add Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1) (1)'))

WebUI.delay(5)

Authorized_Parties = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_Ray HuenHazelCY Leung'))

WebUI.verifyMatch(Authorized_Parties, authorized_party1, false)

//Notes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'), notes)

//Includes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'), includes)

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1) (1)'))

WebUI.delay(3)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Please input Access Kit Name', false)

WebUI.acceptAlert()

WebUI.delay(5)

//------------------------------------------Verify Fields---------------------------------------------------------------------------------------------------
if (type == 'Restricted') {
    //-----------------------------------------------------Verify Fields----------------------------------------------------------------
    //Enable Edit Field
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'Restricted', 0)

    // Notification Parties Fields
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    // Notification Parties List
    Notification_Field1 = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select__2def41devbss.com'))

    WebUI.verifyMatch(Notification_Field1, notification_party_email_1, false)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    // Notification Parties Add Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    //Notification Parties Delete Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)' //-----------------------------------------------------Verify Fields----------------------------------------------------------------
            ) //Enable Edit Field
        ) //Type
    // Notification Parties Fields
    // Notification Parties List
    // Notification Parties Add Button
    //Notification Parties Delete Button
} else {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'TSC_Restricted', 0)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)'))
}

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'))

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'))

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'), 'value', 
    notes, 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'))

//Includes
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'), 'value', 
    includes, 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'))

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1) (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'), 
    'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'))

// Authorized Party List
Authorized_Parties1 = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_Ray HuenHazelCY Leung'))

WebUI.verifyMatch(Authorized_Parties1, authorized_party1, false)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1) (1)'))

// Authorized Party Add Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1) (1)'))

//Authorized Party Delete Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1) (1)'))

WebUI.comment('Test Case: Don\'t input Access Kit Name')

//--------------------------------------------------------------Test Case: Don't input Access Kit Name End---------------------------------------------------------
//=================================================================================================================================================================
//-------------------------------------------------------Test Case: Don't input Authorized Parties --------------------------------------------------------
WebUI.comment('Test Case: Don\'t input Authorized Parties')

//--------------------------------------------------------Input something in Edit Fields-------------------------------------------------------
//Name
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'), name)

/*if(type == "Restricted"){
//Notification Parties
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), notification_party_email_1)

WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

WebUI.delay(5)

Notification_Field = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select__2def41devbss.com'))
WebUI.verifyMatch(Notification_Field, notification_party_email_1, false)

}*/
//Click Delete Button
WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_Ray HuenHazelCY Leung'), authorized_party1, 
    true)

WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1) (1)'))

Authorized_Parties = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_Ray HuenHazelCY Leung'))

WebUI.verifyMatch(Authorized_Parties, '', false)

//Notes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'), notes)

//Includes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'), includes)

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1) (1)'))

WebUI.delay(3)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Please input Authorized Parties', false)

WebUI.acceptAlert()

WebUI.delay(5)

//------------------------------------------Verify Fields---------------------------------------------------------------------------------------------------
if (type == 'Restricted') {
    //-----------------------------------------------------Verify Fields----------------------------------------------------------------
    //Enable Edit Field
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'Restricted', 0)

    // Notification Parties Fields
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    // Notification Parties List
    Notification_Field1 = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select__2def41devbss.com'))

    WebUI.verifyMatch(Notification_Field1, notification_party_email_1, false)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    // Notification Parties Add Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    //Notification Parties Delete Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)' //-----------------------------------------------------Verify Fields----------------------------------------------------------------
            ) //Enable Edit Field
        ) //Type
    // Notification Parties Fields
    // Notification Parties List
    // Notification Parties Add Button
    //Notification Parties Delete Button
} else {
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'TSC_Restricted', 0)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)'))
}

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'))

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'), 'value', 
    name, 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'))

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'), 'value', 
    notes, 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'))

//Includes
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'), 'value', 
    includes, 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'))

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1) (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'), 
    'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'))

// Authorized Party List
Authorized_Parties1 = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_Ray HuenHazelCY Leung'))

WebUI.verifyMatch(Authorized_Parties1, '', false)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1) (1)'))

// Authorized Party Add Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1) (1)'))

//Authorized Party Delete Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1) (1)'))

WebUI.comment('Test Case: Don\'t input Authorized Parties')

//--------------------------------------------------------------Test Case: Don't input Authorized Parties End---------------------------------------------
//=================================================================================================================================================================
//-------------------------------------------------------Test Case: Don't input includes --------------------------------------------------------
WebUI.comment('Test Case: Don\'t input includes')

//--------------------------------------------------------Input something in Edit Fields-------------------------------------------------------
//Name
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'), name + 'c')

//Authorized Parties
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'), authorized_party_prefix1)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Suggestion_List (1)'), 5)

WebUI.delay(7)

WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/suggestion _value_rayhuen (1)'))

// Authorized Party Add Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1) (1)'))

WebUI.delay(5)

Authorized_Parties = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_Ray HuenHazelCY Leung'))

WebUI.verifyMatch(Authorized_Parties, authorized_party1, false)

//Includes
WebUI.clearText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'))

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1) (1)'))

WebUI.delay(7)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Do you confirm to save this Access Kit?', false)

WebUI.acceptAlert()

WebUI.delay(5)

WebUI.comment('Test Case: Don\'t input Authorized Parties')

//--------------------------------------------------------------Test Case: Don't input Authorized Parties End--------------------------------------
//=================================================================================================================================================
//======================================================================================================================
//--------------------------------------------------------------Test Case: Use same Access Kit name with existing record---------------------------------------------------------
WebUI.comment('Test Case: Use same Access Kit name with existing record Start')

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_Password_butLogin'))

//Verify login
WebUI.delay(5)

//Go to Access Kit Configuration Page
WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Access Kit Configuration'))

//Click "Add Access Kit" Button
WebUI.click(findTestObject('Page_AKMS - Access Kit Configuratio/input_Access Kit Configuration'))

WebUI.delay(5)

if (type == 'Restricted') {
    //-----------------------------------------------------UnSuccessful Case : Restricted Type-----------------------------------------
    //Restricted Type
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'Restricted', false)

    //-----------------------------------------------------Verify Fields----------------------------------------------------------------
    //Enable Edit Field
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'Restricted', 0)

    // Notification Parties Fields
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    // Notification Parties List
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    // Notification Parties Add Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    //Notification Parties Delete Button
    WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)' //-----------------------------------------------------UnSuccessful Case : Temporary Staff Card Restricted Type-----------------------------------------
            ) //Temporary Card Type
        ) //-----------------------------------------------------Verify Fields----------------------------------------------------------------
    //Enable Edit Field
    //Type
    // Notification Parties Fields
    // Notification Parties List
    // Notification Parties Add Button
    //Notification Parties Delete Button
} else {
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'TSC_Restricted', false)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'), 
        'value', 'TSC_Restricted', 0)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'))

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'), 
        'value', '', 0)

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1) (1)'))
}

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1) (1)'))

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'))

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'))

//Includes
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'))

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1) (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'), 
    'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1) (1)'), 
    'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1) (1)'))

// Authorized Party Add Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1) (1)'))

//Authorized Party Delete Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1) (1)'))

//--------------------------------------------------------Input something in Edit Fields-------------------------------------------------------
// Verify DB records
//DB Connection
CustomKeywords.'DBConnection.ConnectDB'('UMTLHKSQLA', 'AKMS_AutoTest', '1433')

ResultSet rs = CustomKeywords.'DBConnection.executeQuery'('select TOP(1)  AccessKitName from dbo.AccessKit order by AccessKitId desc')

rs.next()

database_name = rs.getString('AccessKitName')

//Name
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1) (1)'), database_name)

if (type == 'Restricted') {
    //Notification Parties
    WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1) (1)'), 
        notification_party_email_1)

    WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1) (1)'))

    WebUI.delay(5)

    Notification_Field = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select__2def41devbss.com'))

    WebUI.verifyMatch(Notification_Field, notification_party_email_1, false)
}

//Authorized Parties
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1) (1)'), authorized_party_prefix1)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Suggestion_List (1)'), 5)

WebUI.delay(7)

WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/suggestion _value_rayhuen (1)'))

// Authorized Party Add Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1) (1)'))

WebUI.delay(5)

Authorized_Parties = WebUI.getText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_Ray HuenHazelCY Leung'))

WebUI.verifyMatch(Authorized_Parties, authorized_party1, false)

//Notes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1) (1)'), notes)

//Includes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1) (1)'), includes)

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1) (1)'))

WebUI.delay(7)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Do you confirm to save this Access Kit?', false)

WebUI.acceptAlert()

WebUI.delay(5)

WebUI.comment('Test Case: Use same Access Kit name with existing record End')

