import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import java.sql.ResultSet as ResultSet
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_Password_butLogin'))

//Verify login
WebUI.delay(10)

//Go to Access Kit Configuration Page
WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Access Kit Configuration'))

WebUI.delay(10)

//WebUI.click(findTestObject('Page_AKMS - Access Kit Configuratio/b_Access Kit Configuration'))
//Click "Add Access Kit" Button
WebUI.click(findTestObject('Page_AKMS - Access Kit Configuratio/input_Access Kit Configuration'))

WebUI.delay(5)

WebUI.comment(type)

if (type == 'Normal') {
    //-----------------------------------------------------Successful Case : Normal Type-----------------------------------------
    //Temporary Card Type
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'TSC_Normal', false)

    //Change to Normal Type
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'Normal', false)

    //------------------------------Verify Fields---------------------------------------------------------------------------------------------------
    //Enable Edit Field
    //Type
    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'Normal', 0 //-----------------------------------------------------Successful Case : Temporary Card Type-----------------------------------------
        ) //Temporary Card Type
    //------------------------------Verify Fields---------------------------------------------------------------------------------------------------
    //Enable Edit Field
    //Type
} else {
    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'TSC_Normal', false)

    WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'), 
        'value', 'TSC_Normal', 0)
}

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora (1)'))

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'), 'value', '', 
    0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'))

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'), 'value', '', 
    0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'))

//Includes
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), 'value', 
    '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'))

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

//Disable Edit Field
// Notification Parties Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl (1)'))

// Notification Parties List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields (1)'))

// Notification Parties Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button (1)'))

//Notification Parties Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button (1)'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth (1)'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party (1)'))

// Authorized Party Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA (1)'))

//Authorized Party Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button (1)'))

//--------------------------------------------------------Input something in Edit Fields-------------------------------------------------------
//Name
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name (1)'), name)

//Includes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes (1)'), includes)

//Notes
WebUI.setText(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note (1)'), notes)

//Save Button
WebUI.click(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm (1)'))

WebUI.delay(5)

//----------------------------------Verify Alert Text --------------------------------------
Alert_text = WebUI.getAlertText()

WebUI.verifyMatch(Alert_text, 'Do you confirm to save this Access Kit?', false)

WebUI.acceptAlert()

WebUI.delay(5)

// Verify DB records  
//DB Connection
CustomKeywords.'DBConnection.ConnectDB'('UMTLHKSQLA', 'AKMS_AutoTest', '1433')

ResultSet rs = CustomKeywords.'DBConnection.executeQuery'('select TOP(1) AccessKitId, AccessKitName, AccessKitType, Status,ItemIncludes from dbo.AccessKit order by AccessKitId desc')

rs.next()

database_name = rs.getString('AccessKitName')

database_type = rs.getString('AccessKitType')

database_status = rs.getString('Status')

database_includes = rs.getString('ItemIncludes')

WebUI.comment(database_name)

WebUI.comment(database_includes)

WebUI.verifyMatch(type, database_type, false)

WebUI.verifyMatch(name, database_name, false)

WebUI.verifyMatch(database_status, 'Available', false)

WebUI.verifyMatch(includes, database_includes, false)

CustomKeywords.'DBConnection.closeDatabaseConnection'()

WebUI.comment('Verify DB record')

