import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName'), 'AT_AKMS_OAKAdmin')

WebUI.setEncryptedText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), 'LnWi7YciS/v+ZDPH8UK5yqcaBlSdxnFl')

WebUI.click(findTestObject('Page_AKMS - Login/input_Password_butLogin'))

//Verify login
WebUI.delay(5)

//Go to Access Kit Configuration Page
WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Access Kit Configuration'))

WebUI.delay(5)

//Click "Add Access Kit" Button
WebUI.click(findTestObject('Page_AKMS - Access Kit Configuratio/input_Access Kit Configuration'))

WebUI.delay(5)

//------------------------------Verify Default Settings---------------------------------------------------------------------------------------------------
//Enable Edit Field
//Type
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora'), 
    'value', 'Normal', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/select_NormalRestrictedTempora'))

//Name
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name'), 'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Name'))

//Note
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note'), 'value', '', 0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Note'))

//Include
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes'), 'value', '', 
    0)

WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Includes'))

//Save Button
WebUI.verifyElementClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbutSubm'))

//Disable Edit Field
// Notification Parties Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input_Notification Parties_ctl'))

// Notification Parties List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Notification _Fields'))

// Notification Parties Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Add_Button'))

//Notification Parties Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Notification_Party_Delete_Button'))

//Authorized Party Fields
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContenttxtAuth'))

// Authorized Party List
WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party'), 
    'value', '', 0)

WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/List_Authorized_Party'))

// Authorized Party Add Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/input__ctl00MainContentbtnAddA'))

//Authorized Party Delete Button
WebUI.verifyElementNotClickable(findTestObject('Object Repository/Page_AKMS - Add Access Kit/Authorized_Party_Delete_Button'))

