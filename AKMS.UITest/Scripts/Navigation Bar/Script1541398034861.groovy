import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://autotestakms.devbss.com/Login.aspx')

'Login Function\r\n\r\n'
WebUI.setText(findTestObject('Page_AKMS - Login/input_Login ID_txtUserName (1)'), Username)

WebUI.setText(findTestObject('Page_AKMS - Login/input_Password_txtPassword'), Password)

WebUI.click(findTestObject('Page_AKMS - Login/input_Password_butLogin'))

//Verify whether login successfully 
WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Page_AKMS - Borrow  Return/b_Borrow  Return'), 5)

WebUI.verifyElementText(findTestObject('Page_AKMS - Borrow  Return/b_Borrow  Return'), 'Borrow & Return')

//Click and verify Loan History
if (Username == 'AT_AKMS_OAKAdmin') {
    WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Loan History1'))
} else {
    WebUI.click(findTestObject('Page_Default/a_Loan History'))
}

WebUI.verifyElementText(findTestObject('Page_AKMS - Loan History/b_Loan History'), 'Loan History')

WebUI.delay(2)

//Click and verify Borrow & Return
WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Borrow  Return'))

WebUI.verifyElementText(findTestObject('Page_AKMS - Borrow  Return/b_Borrow  Return'), 'Borrow & Return')

WebUI.delay(2)

//Click and verify Assess Kit Configuration 
if (Username == 'AT_AKMS_OAKAdmin') {
    WebUI.click(findTestObject('Page_AKMS - Borrow  Return/a_Access Kit Configuration'))

    WebUI.click(findTestObject('Object Repository/Page_AKMS - Access Kit Configuratio/b_Access Kit Configuration'))
}

WebUI.delay(2)

//Click Download and verify Download Authorized file
WebUI.click(findTestObject('Page_AKMS - Borrow  Return/button_Download Authorized Par'))

WebUI.closeBrowser()

