<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>List_Authorized_Party</name>
   <tag></tag>
   <elementGuidId>ab2d8c56-4a9a-428a-b8db-2aad995d0db0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;MainContent_listAuthorizedParties&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;MainContent_listAuthorizedParties&quot;]</value>
   </webElementProperties>
</WebElementEntity>
