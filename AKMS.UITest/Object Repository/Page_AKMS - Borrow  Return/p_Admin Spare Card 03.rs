<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Admin Spare Card 03</name>
   <tag></tag>
   <elementGuidId>8e7b50b8-8ee0-4e86-a831-13eb70ad9abb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ResultTable&quot;)/tbody[1]/tr[@class=&quot;odd selected&quot;]/td[@class=&quot;dt_blueText&quot;]/p[@class=&quot;ui-li-desc&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-li-desc</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Admin Spare Card 03</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ResultTable&quot;)/tbody[1]/tr[@class=&quot;odd selected&quot;]/td[@class=&quot;dt_blueText&quot;]/p[@class=&quot;ui-li-desc&quot;]</value>
   </webElementProperties>
</WebElementEntity>
