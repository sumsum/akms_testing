<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Borrow  Return</name>
   <tag></tag>
   <elementGuidId>50525e85-4771-45b5-99cb-0293a06f9ef5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form1']/div[4]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
             
             
              Borrow &amp; Return 
             

            
                Access Kit Type:
               
                
	All
	Normal
	Restricted


             
           
            
                Access Kit Status:
                 
	All
	Available
	On-Loan

             
         
            
                Display Temp. Staff Card Only:
                
                 Yes  
                 No
             
         
      


    Show All records per pageSearch in listPrevious1Next
        
            Access Kit TypeAccess Kit NameApplicant NameBorrow TimeDuration
        
        
            
        NormalRm2307 Office Access KitRestrictedRm1011 Staging Room Access KitRestrictedRm1011 Warehouse Access KitRestrictedRm2301 Office Access KitRestrictedRm2303 Store Room Access KitRestrictedRm2305 SOC Access KitRestrictedRm2308 Computer Room Access Kit
    Showing 1 to 7 of 7 entriesPrevious1Next
     
    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form1&quot;)/div[@class=&quot;main&quot;]/div[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//form[@id='form1']/div[4]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download Authorized Parties'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loan History'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[4]/div</value>
   </webElementXpaths>
</WebElementEntity>
