<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Logout</name>
   <tag></tag>
   <elementGuidId>a3ee7dc6-18e0-4c63-bf0f-16d4d3de8579</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;form1&quot;)/div[3]/div[@class=&quot;header&quot;]/div[@class=&quot;title&quot;]/table[1]/tbody[1]/tr[1]/td[3]/a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>Logout.aspx</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Logout</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form1&quot;)/div[3]/div[@class=&quot;header&quot;]/div[@class=&quot;title&quot;]/table[1]/tbody[1]/tr[1]/td[3]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
