<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Access Kit Configuration</name>
   <tag></tag>
   <elementGuidId>06a71933-3d15-4222-bbe1-9aea1cca380d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form1']/div[4]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

         
            Access Kit Configuration    
        
           
          
            
        

      Show All records per pageSearch in listPrevious1Next
        
            Access Kit TypeAccess Kit NameStatusIncludes
        
        
            
        NormalasdfDisabledasdfNormalRm2302 Office Access KitOn-Loan - Access Card x 1 - Guideline x 1 - Key (Gate, Meeting Room) x 2NormalRm2304 Office Access KitOn-Loan - Access Card x 1 - Guideline x 1 - Key (Glass Door) x 1NormalRm2306 Office Access KitOn-Loan - Access Card x 1 - Guideline x 1 - Key (Gate) x 1NormalRm2307 Office Access KitAvailable - Access Card x 1 - Guideline x 1 - Key (Glass Door) x 1NormalRm2308 Office Access KitOn-Loan - Access Card x 1 - Guideline x 1 - Key (Glass Door) x 1RestrictedRm1011 Staging Room Access KitAvailable- Access Card x 1 - Guideline x 1 - Key (Chain Lock, Wooden Door, Gate) x 3RestrictedRm1011 Warehouse Access KitAvailable- Access Card x 1 - Guideline x 1 - Key (Gate) x 1RestrictedRm2301 Office Access KitAvailable- Access Card x 1 - Guideline x 1 - Key (Glass Door, Gate) x 2RestrictedRm2301 Server Room Access KitOn-Loan - Access Card x 1 - Guideline x 1 - Key (Wooden Door) x 1 - Server Rack Key x 5RestrictedRm2302 Store Room Access KitOn-Loan - Access Card x 1 - Guideline x 1 - Key (Wooden Door) x 1RestrictedRm2303 Store Room Access KitAvailable - Key (Wooden Door) x 1RestrictedRm2305 SOC Access KitAvailable - Access Card x 1RestrictedRm2306 Store Room Access KitOn-Loan- Guideline x 1 - Key (Wooden Door) x 1RestrictedRm2308 Computer Room Access KitAvailable - Key (Wooden Door) x 1Temporary Staff CardAdmin Spare Card 01On-Loan - Access Card x 1Temporary Staff CardAdmin Spare Card 02Available - Access Card x 1Temporary Staff CardAdmin Spare Card 03Available - Access Card x 1Temporary Staff CardAdmin Spare Card 04On-Loan - Access Card x 1Temporary Staff CardAdmin Spare Card 05Available- Access Card x 1Temporary Staff CardAdmin Spare Card 07Available - Access Card x 1Temporary Staff CardAdmin Spare Card 08Available - Access Card x 1Temporary Staff CardAdmin Spare Card 09Available - Access Card x 1Temporary Staff CardAdmin Spare Card 10Available - Access Card x 1Temporary Staff CardRm1906 Office Access KitAvailable- Access Card x 1 - Guideline x 1 - Key (Glass Door) x 1Temporary Staff CardSOC Spare (Experience Lab)Available - Access Card x 1Temporary Staff Card (Restricted)Admin Spare Card 06Available - Access Card x 1Temporary Staff Card (Restricted)Admin Spare Card 9901Availableaccess card no. 99601Temporary Staff Card (Restricted)hihihiAvailableasdasdsads
    Showing 1 to 29 of 29 entriesPrevious1Next

     
    

</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form1&quot;)/div[@class=&quot;main&quot;]/div[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//form[@id='form1']/div[4]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download Authorized Parties'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loan History'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[4]/div</value>
   </webElementXpaths>
</WebElementEntity>
