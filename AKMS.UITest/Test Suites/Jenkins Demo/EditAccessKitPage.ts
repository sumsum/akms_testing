<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>EditAccessKitPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-09T21:00:22</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>dd6db3e6-1f89-43de-a0dd-220078596e59</testSuiteGuid>
   <testCaseLink>
      <guid>ef368d87-1beb-4df4-bc23-8a74b1ddcb7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Default_Setting_Available_Access_Kit_Normal_Temporary</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>5c18a90d-5986-480b-8732-34c7cd0503d6</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-2</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>5c18a90d-5986-480b-8732-34c7cd0503d6</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>9e92758b-2c5a-407f-a0a9-ededa45d0fb8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c43dcaa0-4edc-4040-b20e-f6d50c148353</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Default_Setting_On-Loan_Access_Kit</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f0abe3df-25d4-44c5-a487-981360410a63</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value>4-4</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>f0abe3df-25d4-44c5-a487-981360410a63</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>689e1fed-e3c6-4ef4-973c-6c6da59c48e1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>df774443-0ea1-44ea-a742-45671d2e3b4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Default_Setting_Available_Access_Kit_Restricted_Temporary_Restricted</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>152838a0-2d7d-461d-b1b9-25ae7854cfea</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-4</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>152838a0-2d7d-461d-b1b9-25ae7854cfea</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>4f7387ee-230a-4cea-9485-4c05c7ee025e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8cfa23b4-63d3-4488-a4fe-79ca62ede375</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Successful_Enable_Case_Normal_Temporary_to_Restricted_AND_Temporary_Restricted</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0284610e-e796-48e9-a798-7c30517717bd</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-2</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0dc34bb0-c2a8-41fe-bba3-39ea885577db</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>0284610e-e796-48e9-a798-7c30517717bd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>2f5bec0e-907b-434c-8cd4-4c822896a6a1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0dc34bb0-c2a8-41fe-bba3-39ea885577db</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>e6879e15-5542-4550-b4b3-51ee1694f6ef</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0dc34bb0-c2a8-41fe-bba3-39ea885577db</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>0cb4c870-9922-44e0-9364-17c0fd34fe30</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0dc34bb0-c2a8-41fe-bba3-39ea885577db</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>532a1b74-2cfa-406f-9f10-bdeadb44ee5f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0dc34bb0-c2a8-41fe-bba3-39ea885577db</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix1</value>
         <variableId>31fa51fd-527e-4a7a-8df8-b7d5ebe9975c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0dc34bb0-c2a8-41fe-bba3-39ea885577db</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party1</value>
         <variableId>e579af1c-a730-4f4f-a887-d9f8bad800d5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0dc34bb0-c2a8-41fe-bba3-39ea885577db</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notificaiton Party Email</value>
         <variableId>5c3cfb1d-7e48-4049-a568-4256c10a2589</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0dc34bb0-c2a8-41fe-bba3-39ea885577db</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Next Notificaiton Party Email</value>
         <variableId>1ba33163-911a-4a30-84f9-d072ff45256c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0dc34bb0-c2a8-41fe-bba3-39ea885577db</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix2</value>
         <variableId>4cfab77a-6fcb-4ab7-b07f-55376fc5c227</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0dc34bb0-c2a8-41fe-bba3-39ea885577db</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party2</value>
         <variableId>0197673d-ef69-463c-a664-3c85a207b5ab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c5fa00cb-f7ff-4908-a999-11522cc9733b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Successful_Enable_Case_Restricted_Temporary_Restricted_to_Normal_AND_Temporary</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>04309b51-e464-4ca1-b16e-41de0de78bdf</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-4</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>04309b51-e464-4ca1-b16e-41de0de78bdf</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>7f77ebf9-52e2-4380-955d-afd13ac02e29</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>96bb8911-0a83-4fca-a588-001fbe963bc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Successful_Enable_Case_Normal_Temporary_to_Temporary_Normal</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1d791be8-3cfa-4570-9c5d-716e13292f1a</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-2</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>1d791be8-3cfa-4570-9c5d-716e13292f1a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>70a8275f-cc21-452e-8e75-a1e98799171a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0f2ba7c0-950a-49d7-b217-59b0a80474d9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>88445b21-6e74-46d5-9281-96af52097975</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>018db366-615a-492f-acb7-20606be72040</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e4748214-d370-4bd6-a918-6de580a65918</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Successful_Enable_Case_ Restricted_Temporary_Restricted_ to_Temporary_Restricted_Restricted</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>424756ea-8f38-408b-90ca-f3c877ded5b9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>fdc06559-ada7-49f2-b41d-87947e12f39f</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-4</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>fdc06559-ada7-49f2-b41d-87947e12f39f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>5340cb10-ebdc-4b5b-9919-b4dd66215354</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>424756ea-8f38-408b-90ca-f3c877ded5b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>dec70d49-1662-4ade-8469-6136d4f03536</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>424756ea-8f38-408b-90ca-f3c877ded5b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>c316d7c4-e3c3-4b47-bf52-35e5e2925c61</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>424756ea-8f38-408b-90ca-f3c877ded5b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>c84661f0-fcf1-48e9-a24b-416cea17cafc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>424756ea-8f38-408b-90ca-f3c877ded5b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix1</value>
         <variableId>3c214f31-67cb-462f-a72c-3053b196d5e8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>424756ea-8f38-408b-90ca-f3c877ded5b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party1</value>
         <variableId>78aebdb2-57ea-4008-8c08-2e16c4db0873</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>424756ea-8f38-408b-90ca-f3c877ded5b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notificaiton Party Email</value>
         <variableId>1ee217fe-43e0-4490-be2b-6cf8ae8cdab2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>424756ea-8f38-408b-90ca-f3c877ded5b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Next Notificaiton Party Email</value>
         <variableId>6fc9fd1d-1d68-4d5b-962d-2d2f0d4e3494</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>424756ea-8f38-408b-90ca-f3c877ded5b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix2</value>
         <variableId>db74222f-c852-4a16-a0a6-34ae759b3e20</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>424756ea-8f38-408b-90ca-f3c877ded5b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party2</value>
         <variableId>7b1db27b-c50e-4d5b-86e4-4171dbf718c9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9586b522-3268-417b-bdb6-6189a427cfb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Successful_Disable_Case_Normal_Temporary_to_Temporary_Normal</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f2262fee-9335-4393-8c41-b4de18f5310a</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-2</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>f2262fee-9335-4393-8c41-b4de18f5310a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>6e0da0c9-a6b8-44e0-8e9d-881ace042583</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ea012e57-a71c-48ca-bd49-08094ff029b4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a843cdf1-137e-4acf-a86a-5bdacc4e935d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>045cd15e-b9d0-42cf-bd92-92de5f98da3c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>03a08d00-3a05-42fa-893d-7d1fad6d3a1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Successful_Disable_Case_ Restricted_Temporary_Restricted_ to_Temporary_Restricted_Restricted</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2a1d1697-7fd1-4556-9d4e-8d2d3ad5cc9b</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-4</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>021b70d5-5fd1-4720-aa69-72b9b9649384</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>2a1d1697-7fd1-4556-9d4e-8d2d3ad5cc9b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>3f17b9ca-2006-4adb-9007-15fbdca7bc30</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>021b70d5-5fd1-4720-aa69-72b9b9649384</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>352df356-5683-4799-a930-4770acec2f06</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>021b70d5-5fd1-4720-aa69-72b9b9649384</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>cedff59c-5835-4a77-8c65-54b548d292f8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>021b70d5-5fd1-4720-aa69-72b9b9649384</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>fedb1d6f-a296-47ea-8339-df66278bce8e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>021b70d5-5fd1-4720-aa69-72b9b9649384</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix1</value>
         <variableId>a95f5696-90b2-4f93-a64a-5b5e9076b358</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>021b70d5-5fd1-4720-aa69-72b9b9649384</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party1</value>
         <variableId>0e7a0749-3843-4612-8922-69ba867953c2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>021b70d5-5fd1-4720-aa69-72b9b9649384</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notificaiton Party Email</value>
         <variableId>990e11fe-801a-4657-871b-a34988a91355</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>021b70d5-5fd1-4720-aa69-72b9b9649384</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Next Notificaiton Party Email</value>
         <variableId>ca5c255e-39b8-4834-b10b-7a4f625d3b92</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>021b70d5-5fd1-4720-aa69-72b9b9649384</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix2</value>
         <variableId>79e17c12-3104-4578-b4ba-5b95a7a77d12</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>021b70d5-5fd1-4720-aa69-72b9b9649384</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party2</value>
         <variableId>c162f865-e3d4-4e44-9d06-0d3e84debedd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f254dc8c-2237-43df-b73a-b979688cfa85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Successful_Disable_Case_Normal_Temporary_to_Restricted_AND_Temporary_Restricted</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>afa9a5ff-2c7d-4c81-992e-0983def63ced</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-2</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ec06bd2c-8d86-434d-893b-60866d21e670</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>afa9a5ff-2c7d-4c81-992e-0983def63ced</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>27197b9b-bbc4-4de5-bfcd-676661387ccf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ec06bd2c-8d86-434d-893b-60866d21e670</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>0032771e-030d-4b37-8f24-761cd9f02672</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ec06bd2c-8d86-434d-893b-60866d21e670</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>585c62a2-e7a9-466f-acea-a1ba97e67ae2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ec06bd2c-8d86-434d-893b-60866d21e670</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>86fed212-48d4-4256-9180-567741f7cc62</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ec06bd2c-8d86-434d-893b-60866d21e670</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix1</value>
         <variableId>7d418d5c-fa5f-4564-b2f5-755c0cde9b5b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ec06bd2c-8d86-434d-893b-60866d21e670</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party1</value>
         <variableId>5da74870-681e-478b-96d1-221d278349b9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ec06bd2c-8d86-434d-893b-60866d21e670</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notificaiton Party Email</value>
         <variableId>cbcf3cf4-85b7-49ed-a0d0-abaf4fb83f3e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ec06bd2c-8d86-434d-893b-60866d21e670</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Next Notificaiton Party Email</value>
         <variableId>6e1bf8ee-6f75-403b-b0c9-f084b472342f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ec06bd2c-8d86-434d-893b-60866d21e670</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix2</value>
         <variableId>2e302d4b-e8af-4cde-b8f4-da2eafcef131</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ec06bd2c-8d86-434d-893b-60866d21e670</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party2</value>
         <variableId>5f096260-c5c3-4c46-9c0c-bc2b0c137742</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1d3bb1ae-1d5f-4657-94d5-949c3f40467b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Successful_Disable_Case_Restricted_Temporary_Restricted_to_Normal_AND_Temporary</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a9dc9520-563c-4610-9b21-17e2e517b03e</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-4</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>a9dc9520-563c-4610-9b21-17e2e517b03e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>308c3d8b-12e6-4b53-b29f-be9786dd8505</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>37e1aad2-f2af-4ded-b5cf-1326a1fee77d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Unsuccessful_Case_ Normal_Temporary_Edit_Access_Kit</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b01b5456-b25d-4324-9469-03bad89c9093</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-2</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7c230240-0652-47f3-bef4-13d9e66eebc0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>b01b5456-b25d-4324-9469-03bad89c9093</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>0694c195-d51d-4886-a56e-9b0c267c29cd</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7c230240-0652-47f3-bef4-13d9e66eebc0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>b1763808-9553-44d0-8dda-e56efd50ea12</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7c230240-0652-47f3-bef4-13d9e66eebc0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>dafe4018-cd9d-419d-bda3-2e961d0dcbdf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7c230240-0652-47f3-bef4-13d9e66eebc0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>b8142af4-3cec-4e1f-844e-a49bfb2eec47</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>acee4208-b8fa-481f-a330-6cffc4f07f5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Edit Access Kit Page/Unsuccessful_Case_Restricted_Temporary_Restricted_Edit_Access_Kit</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4b679390-8901-404d-9223-3bd61cddcd77</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-4</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8449bffd-b39f-47ba-a579-bfcedcb41cc8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>4b679390-8901-404d-9223-3bd61cddcd77</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type (Edit Access Kit)</value>
         <variableId>df6b769a-c7a2-41b9-9ff3-27a4fe7264f6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8449bffd-b39f-47ba-a579-bfcedcb41cc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>fe090ca5-bc7f-4153-b5e7-912f19434fbc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8449bffd-b39f-47ba-a579-bfcedcb41cc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>de8abbdd-d28e-404a-8bcc-b10321300169</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8449bffd-b39f-47ba-a579-bfcedcb41cc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>f1b2237d-91fb-4793-9d83-55fbe3259b71</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8449bffd-b39f-47ba-a579-bfcedcb41cc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix1</value>
         <variableId>29127696-ecbe-44c0-b181-1aa388aa1b4b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8449bffd-b39f-47ba-a579-bfcedcb41cc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party1</value>
         <variableId>ce3d5dc4-ad98-4349-9281-6a9b150bbc83</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8449bffd-b39f-47ba-a579-bfcedcb41cc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notificaiton Party Email</value>
         <variableId>47d48fcb-009f-4bee-b910-c477af74688b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8449bffd-b39f-47ba-a579-bfcedcb41cc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Next Notificaiton Party Email</value>
         <variableId>1d4d6f09-fb06-46b2-9af5-97233f0a4675</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8449bffd-b39f-47ba-a579-bfcedcb41cc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix2</value>
         <variableId>fc532eee-e2a4-4cff-9be2-d76671130421</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8449bffd-b39f-47ba-a579-bfcedcb41cc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party2</value>
         <variableId>60591cbb-edf9-42cc-a7d6-d4407a2fcd61</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
