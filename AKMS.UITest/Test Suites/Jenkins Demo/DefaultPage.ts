<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>DefaultPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-09T21:00:16</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ce41c04c-8453-4cb2-80d0-7721659eed6e</testSuiteGuid>
   <testCaseLink>
      <guid>30cb4ae6-8e37-471a-b682-3f6357d7d320</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/CheckDisplayName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d35a008-4294-4d6d-bc94-f327ff53778e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/VerifyDefaultSetting/VerifyDefaultSetting_LoanOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e4b3993-314a-437f-8b69-05f6677477b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/VerifyDefaultSetting/VerifyDefaultSetting_OAKAdmin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81db3193-3c5f-4cf7-b3d0-3ab4dce50924</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/VerifyDefaultSetting/VerifyDefaultSetting_ReceptionOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87bc13f1-393c-4491-861e-ed53869996a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Ascending/AccessKitTableSorting_Ascending_AccessKitName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1755e65-f96e-4535-a3d8-b0e17283f522</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Ascending/AccessKitTableSorting_Ascending_AccessKitType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43e06117-0ae7-44e5-83bc-4eacbad71b90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Ascending/AccessKitTableSorting_Ascending_ApplicantName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0d2cacf-c6c1-47c9-a3a1-d09327af61a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Ascending/AccessKitTableSorting_Ascending_BorrowTime</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3f2bc5a-cdda-491a-becc-e6f1fbb9cfa4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Ascending/AccessKitTableSorting_Ascending_Duration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c12f5c1-307f-4bf3-8fe1-dc38e4d3ad93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Descending/AccessKitTableSorting_Descending_AccessKitName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0327341-eefc-4c72-95d8-0ecfbd5c40ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Descending/AccessKitTableSorting_Descending_AccessKitType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>467af560-b956-4d43-be01-19bb95ca05d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Descending/AccessKitTableSorting_Descending_ApplicantName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a56afe58-4ef4-4c8c-b7fe-3bfcd0160fe0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Descending/AccessKitTableSorting_Descending_BorrowTime</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>269b286a-9944-4c4f-bd1e-5a76321e388b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/AccessKitTableSorting_Descending/AccessKitTableSorting_Descending_Duration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb58f116-c658-439d-b80a-206e780adeba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchBy DisplayTempStaffCardOnlyOnly/SearchBy DisplayTempStaffCardOnlyOnly_LoanOperator_Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>024d2c36-0add-4ba0-81a5-82b8b848975b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchBy DisplayTempStaffCardOnlyOnly/SearchBy DisplayTempStaffCardOnlyOnly_ReceptionOperator_No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4b67292-5745-4fa9-bf24-4cb3c13a490d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitStatusOnly/SearchByAccessKitStatusOnly_Aailable_LoanOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2caee9f8-53dd-4c5d-ae8a-c64824a901a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitStatusOnly/SearchByAccessKitStatusOnly_Aailable_ReceptionOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16f44639-5d9e-4d9c-998c-32be8415e607</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitStatusOnly/SearchByAccessKitStatusOnly_OnLoan_LoanOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>613c35d8-f473-4c17-a396-e0ee2134b1b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitStatusOnly/SearchByAccessKitStatusOnly_OnLoan_ReceptionOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39ede5e2-152c-4e76-8e18-a1bf01142208</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitTypeOnly/SearchByAccessKitTypeOnly_Normal_LoanOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a4bf7f2-b7a8-4eef-a29c-e2f1675176b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitTypeOnly/SearchByAccessKitTypeOnly_Normal_ReceptionOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5db5c17-1627-4cd5-bb6a-c39566479b6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitTypeOnly/SearchByAccessKitTypeOnly_Restricted_LoanOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35f9e529-5380-49eb-95c5-9e397c473e29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitTypeOnly/SearchByAccessKitTypeOnly_Restricted_ReceptionOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2385620f-b465-43a0-b4a0-f6a12c353bd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitTypeOnly/SearchByAccessKitTypeOnly_TemporaryStaffCard_ReceptionOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2c80318-c038-4c31-a4c7-f283da85e813</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchByAccessKitTypeOnly/SearchByAccessKitTypeOnly_TemporaryStaffCard_Restricted_ReceptionOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef66dde1-0555-4440-8014-dff372715a25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchBySearchInListOnly/SearchBySearchInListOnly_SpecialCharacters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8712fcdf-f0ec-4e0a-839d-f0f1a9324f45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchBySearchInListOnly/SearchBySearchInList_CaseSenitive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e588b44f-76ec-44e4-9c96-52f67beccab3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchBySearchInListOnly/SearchBySearchInList_WildCard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78b92e04-f418-4eeb-8fde-c3038257e324</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchBySearchInListOnly/SearchBySearchListOnly_ChineseCharacters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>335424f7-a143-491b-aa0c-93d5ce9c7242</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchBySearchInListOnly/SearchBySearchListOnly_Digits</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9b62f74-013d-4093-b869-dd931ac3f3e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/DefualtPage/SearchBySearchInListOnly/SearchBySearchListOnly_EnglishCharacters</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
