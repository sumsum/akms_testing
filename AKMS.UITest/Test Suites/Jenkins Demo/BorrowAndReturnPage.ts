<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BorrowAndReturnPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-09T21:00:16</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>32a83d76-b32a-46c8-b6ec-d8ed427a3aa7</testSuiteGuid>
   <testCaseLink>
      <guid>e2533daa-f2c8-4509-b8a4-d79e18f15a64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/BlockedBorrow_Normal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eaaeb680-1829-43c6-a9ad-06d700bbbf37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/BlockedBorrow_Restricted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83c602fb-8ac2-4f3f-b096-a0320cda83e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/NormalBorrow_Normal_LoanOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ec7a1e3-893a-49ac-9307-64cbd201c840</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/NormalBorrow_Restricted_LoanOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f764a8cd-cf70-4d33-9c63-673d9413108e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/NormalBorrow_TemporaryStaffCard(Restricted)_ReceptionOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>807231d2-d778-4690-959f-746e2ef8629a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/NormalBorrow_TemporaryStaffCard_ReceptionOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63d1d1dd-7941-4eaa-a38d-05df1f0dc977</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/BorrowSuccess/BorrowSuccess_Normal</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4eb77347-141b-4fc7-9a05-28c67c7fa6eb</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>4eb77347-141b-4fc7-9a05-28c67c7fa6eb</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type</value>
         <variableId>48407ab7-4034-4e0e-bb73-2e829aaa320a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f04f2413-5e0b-40d7-82c5-259e64e1e6be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/BorrowSuccess/BorrowSuccess_Restricted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0df8b862-0250-4179-abab-9e09187baab2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/BorrowSuccess/BorrowSuccess_TemporaryCard(Restricted)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c9980df-3fa1-4f09-b548-15d33a2e0193</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/BorrowSuccess/BorrowSuccess_TemporayCard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b4d3843-a55b-4492-aada-846174c8e435</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/VerifyAccessKitDetails</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28e9628b-000f-4717-b79e-19acfe7c33c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/BorrowFailure/BorrowFailure_LoanOperator_IncorrectUserName_PWD</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e7654b1-0757-4a12-937d-159ad7677699</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/BorrowPage/BorrowFailure/BorrowFailure_ReceptionOperator_OAKAdmin_IncorrectUserName_PWD</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e1a990cd-86c5-4749-8d26-2b01114d25de</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>2-3</value>
         </iterationEntity>
         <testDataId>Data Files/Role_data_xslx</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>e1a990cd-86c5-4749-8d26-2b01114d25de</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>User</value>
         <variableId>b3a7dba5-c03c-413d-b9a3-a986643e51eb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e1a990cd-86c5-4749-8d26-2b01114d25de</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>34b09f13-2c75-41ec-978f-fc6e76fef23b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ffca4201-8778-438d-88c0-a7554a2e21ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/ReturnPage/BlockedToReturn/BlockedToReturn_OAKAdmin_ReceptionOperator</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>42cccc5c-f1b6-4f94-82ac-4d3fe6dc5c92</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>2-3</value>
         </iterationEntity>
         <testDataId>Data Files/Role_data_xslx</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>42cccc5c-f1b6-4f94-82ac-4d3fe6dc5c92</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>User</value>
         <variableId>a69f1401-61bd-49a0-950f-39f281b9db4f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>42cccc5c-f1b6-4f94-82ac-4d3fe6dc5c92</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>6a4f38d7-fdaf-41b2-8a6d-9958d7713918</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>43c0713f-e1d1-46d8-964d-a039ade97199</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/ReturnPage/ResturnFailure/ReturnFailure_incorrectPWD_LoanOperator</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c251815b-583e-491f-ac66-f4f76cf32056</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>deb06264-1d54-4a53-adf9-d3c3fb01037b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e3ac1e59-8bee-4d1d-b9f3-e04f9c8a6f58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/ReturnPage/ResturnFailure/ReturnFailure_incorrectPWD_ReceptionOperator_OAKAdmin</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7da2ad43-257f-4679-bf57-e0846779b0d5</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>2-3</value>
         </iterationEntity>
         <testDataId>Data Files/Role_data_xslx</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>7da2ad43-257f-4679-bf57-e0846779b0d5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>User</value>
         <variableId>86e7bd39-0a85-4666-8b48-6f50be3c2832</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7da2ad43-257f-4679-bf57-e0846779b0d5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>e901c12a-41c9-4277-8570-83096eae65e4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d85653ee-da36-4b2c-846a-0ee7efa7b50d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/ReturnPage/ResturnFailure/ResturnFailure_sameUserAccount_LoanOperator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38ef9a7e-fdec-4704-9dd0-de92fc00ac74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/ReturnPage/ResturnFailure/ResturnFailure_sameUserAccount_RecetionOperator_OAKAdmin</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8f012893-a5fa-432a-98a4-db1e8cfa0085</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>2-3</value>
         </iterationEntity>
         <testDataId>Data Files/Role_data_xslx</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>8f012893-a5fa-432a-98a4-db1e8cfa0085</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>User</value>
         <variableId>311603ec-ae90-4cbf-a65b-8ed49de74c81</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8f012893-a5fa-432a-98a4-db1e8cfa0085</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>b2e7caca-fad6-4927-aa21-ca9d0337d44e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>af98ddfd-1f8f-4c7b-90cc-abca5855f79e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/ReturnPage/CancelSuccess/CancelSuccess</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b3bbc720-3469-45da-bb87-884cf29cdd38</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-3</value>
         </iterationEntity>
         <testDataId>Data Files/Role_data_xslx</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>b3bbc720-3469-45da-bb87-884cf29cdd38</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>User</value>
         <variableId>8fee5d73-87b3-462c-bd2a-c03326493f27</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b3bbc720-3469-45da-bb87-884cf29cdd38</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>d53463ba-94a3-409d-a5f1-61bf37adf18a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>20f7ed48-cdee-4a53-8a1b-ab8589a79735</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/BorrowAndReturnPage/ReturnPage/CancelFailure/CancelFailure</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>52b96204-6466-4ac2-be92-9ec6e97046c9</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-3</value>
         </iterationEntity>
         <testDataId>Data Files/Role_data_xslx</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>52b96204-6466-4ac2-be92-9ec6e97046c9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>User</value>
         <variableId>cd18145b-0fe8-4306-b88e-0c17a5665ee9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>52b96204-6466-4ac2-be92-9ec6e97046c9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>1ecf1d88-59ab-469f-9d95-8e9a5195558f</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
