<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Web API</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-09T21:00:27</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c76a56cd-e4fc-43f9-b610-407e5ad887a1</testSuiteGuid>
   <testCaseLink>
      <guid>c3187e6d-f283-4586-a94b-731aeab7c80a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/API Test/GetBorrowAndReturnList</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2277cac0-3eec-4846-b17f-550ec93c8fb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/API Test/GetUserDisplayNameBySearch</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>50b8849b-4645-4020-bdef-936e7543117c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2a3f6b90-76fa-4d97-9e92-583a8f1fc4a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/API Test/GetUserDisplayNameBySearchSimplified</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c56cbff-223c-46a9-af32-02a919464de0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/API Test/GetUserDisplayNameBySearchTraditional</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
