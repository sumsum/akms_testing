<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>AddAccessKitPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-09T21:00:22</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>32917d6d-618f-4bb5-8c26-55719d30297a</testSuiteGuid>
   <testCaseLink>
      <guid>50e63196-3294-46d3-83aa-dcd47e1a550c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Add Access Kit Page/Default_Setting</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10222bea-b3fb-4ed0-b924-09c9e0f993f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Add Access Kit Page/Successful_Case_Normal_Temporary</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e48b291c-60fc-42b1-bff2-003d02a6e318</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-2</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c93c72ad-0eff-4977-ab9d-462860384aab</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>e48b291c-60fc-42b1-bff2-003d02a6e318</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type</value>
         <variableId>17ca6c79-5338-4a56-91b6-1fb67de07a48</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c93c72ad-0eff-4977-ab9d-462860384aab</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>8166c53d-dc07-4d4e-8ba5-4039e10a038e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c93c72ad-0eff-4977-ab9d-462860384aab</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>eb17ea1f-7efd-4e36-9315-b053d4d1d63c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c93c72ad-0eff-4977-ab9d-462860384aab</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>20bc3f4f-9c32-4c77-af0b-0c0825278574</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>de4a4859-4365-4316-9124-9543eb8a2c20</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9fbbdb3f-28c1-4b33-bd04-b95a2d5a5a08</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>24898585-21e8-4f5d-8eaa-53fa9cd69e46</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>255bc4b5-b6c3-459a-8fd8-04ca7dc04986</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6c4d016c-17d8-4094-b2a6-f2d641cfaff8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Add Access Kit Page/Successful_Case_ Restricted_Temporary_Restricted</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e5e1e416-09b2-45bf-8939-4fe0389ef91c</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-4</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6481f8e9-6359-48af-990c-eb770180b5ec</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>e5e1e416-09b2-45bf-8939-4fe0389ef91c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type</value>
         <variableId>a5b6d768-d072-471e-9cf4-1539e57081f2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6481f8e9-6359-48af-990c-eb770180b5ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>88a3e87a-fc98-41c2-9a86-971c33dbf886</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6481f8e9-6359-48af-990c-eb770180b5ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>75beac7e-07b7-47e8-a6c7-2b589c516bd5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6481f8e9-6359-48af-990c-eb770180b5ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>0b27e31d-3552-49ab-9f32-b7138fa7185f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6481f8e9-6359-48af-990c-eb770180b5ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix1</value>
         <variableId>68a92141-af92-4464-86b5-9bc32089b23d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6481f8e9-6359-48af-990c-eb770180b5ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party1</value>
         <variableId>427df615-9106-4e74-9bb2-f679b6f2f36c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6481f8e9-6359-48af-990c-eb770180b5ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notificaiton Party Email</value>
         <variableId>bb96f31b-a871-4406-921d-bdd8e46f862a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6481f8e9-6359-48af-990c-eb770180b5ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Next Notificaiton Party Email</value>
         <variableId>383ef969-74b0-4c95-a896-7994b5922636</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6481f8e9-6359-48af-990c-eb770180b5ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix2</value>
         <variableId>f0a11770-ebde-4abe-9f25-c2fdaac8eded</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6481f8e9-6359-48af-990c-eb770180b5ec</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party2</value>
         <variableId>b5883468-8b7c-4f91-896f-bc7c27997d05</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cd0ec1b1-e9fb-4169-8b37-6b51d292d118</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Add Access Kit Page/Unsuccessful_Case_ Normal_Temporary_Add_Access_Kit</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3450356f-a810-4c66-9a0e-b92ed0ef9feb</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-2</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1571991b-6a10-4693-a7e8-8407c1c14653</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>3450356f-a810-4c66-9a0e-b92ed0ef9feb</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type</value>
         <variableId>a748b85a-9822-4df1-a5f4-d55d4fc09546</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1571991b-6a10-4693-a7e8-8407c1c14653</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>e97f791a-0738-4de9-966a-fcf519113cef</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1571991b-6a10-4693-a7e8-8407c1c14653</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>2e860af3-841f-49da-9bb8-f6054fb9099c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1571991b-6a10-4693-a7e8-8407c1c14653</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>f422ac15-196e-4b78-8217-53023b86a660</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cb75a6eb-c32a-4c3d-8749-f4cac6787374</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/Add Access Kit Page/Unsuccessful_Case_Restricted_Temporary_Restricted</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>14853d7e-a4c2-4b0f-8036-483b51a3ab74</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-4</value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Type</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7801af2b-5cbf-40e7-9813-f684bfddf77d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Access Kit Information</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>14853d7e-a4c2-4b0f-8036-483b51a3ab74</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Type</value>
         <variableId>fe1b9c7c-1b08-47eb-8446-c6250d0e0421</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7801af2b-5cbf-40e7-9813-f684bfddf77d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Access Kit Name</value>
         <variableId>58e3d859-25c5-43dc-b043-07b5a53e0ebe</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7801af2b-5cbf-40e7-9813-f684bfddf77d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Includes</value>
         <variableId>bb50c3c8-d665-402f-8bf1-731e615fe717</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7801af2b-5cbf-40e7-9813-f684bfddf77d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notes</value>
         <variableId>c5a2feb8-a347-48e1-ac63-204cfd450752</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7801af2b-5cbf-40e7-9813-f684bfddf77d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix1</value>
         <variableId>224865b7-5f1f-46e5-9ce3-a73774718c23</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7801af2b-5cbf-40e7-9813-f684bfddf77d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party1</value>
         <variableId>dc61375f-d499-47ad-84ba-1dad2ffd7522</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7801af2b-5cbf-40e7-9813-f684bfddf77d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Notificaiton Party Email</value>
         <variableId>9221ca6f-2acf-476d-ac91-92436f884749</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7801af2b-5cbf-40e7-9813-f684bfddf77d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Next Notificaiton Party Email</value>
         <variableId>188836da-5dac-47cd-af75-d857f2d819b2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7801af2b-5cbf-40e7-9813-f684bfddf77d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorized Party Prefix2</value>
         <variableId>2c5ce1a3-2007-42e9-bf66-891d7247598a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7801af2b-5cbf-40e7-9813-f684bfddf77d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Authorizede Party2</value>
         <variableId>19dcfc32-78b8-4caa-b134-62700b5f65dc</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
