<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>AccessKitConfigurationPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-09T21:00:17</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>52f74312-8fac-4fb7-83ab-8188e7b01a80</testSuiteGuid>
   <testCaseLink>
      <guid>da238afa-be32-4524-91c3-c99514bc91ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Demo/AccessKitConfigurationPage/ResultTableSorting/ResultTableSorting_Ascending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1598c988-d7d5-41ad-bdce-5e94519e5674</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Demo/AccessKitConfigurationPage/ResultTableSorting/ResultTableSorting_descending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a1abff9-a7fd-46e4-b71f-ec1931c486bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/AccessKitConfigurationPage/SearchListTesting</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31cdc9b7-2ac3-4182-8505-e0846b6f84d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo/AccessKitConfigurationPage/VerifyDefaultSetting/VerifyDefaultSetting</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
